# Workout Odyssey

Check it out [here](https://www.workoutodyssey.com)!

Workout Odyssey helps people stay organized and motivated with their workouts. The app makes it easy to track your progress and see how you're improving over time.

## Features

-   create, edit, and track workouts for 3 different activities: running, walking, cycling
-   view workout progress over time with detailed statistics
-   import workout data directly from TCX files
-   view the workout location on Google maps

## Overview

Workout Odyssey is a Remix front-end app for the [Workouts API](https://gitlab.com/mariuszlas/workouts-java-api). Chart.js is library of choice used for rendering charts on HTML5 Canvas with great performance. The end-to-end test are automated with Playwright.

## Development

### Prerequisites

-   `git`
-   `nodejs`
-   `npm`

A `.env` file must be placed in the root directory with several environment vriables (check the `.env.example` file).

### Installation

1. Run `npm install` from the root directory to install all dependencies including dev dependencies
2. `npm run dev` to create and start a development build server on port `3000`

### Tests

#### Unit Tests

The unit test suit can be run with `npm test` and the coverage with `npm test:coverage`

#### E2E Tests

The the end-to-end tests can be run with `npm run test:e2e-local` which starts the mock server and runs all the Playwright tests. In case of any failuers, a report can be accessed at `http://localhost:9323/`.
