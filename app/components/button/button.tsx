import type { ButtonHTMLAttributes } from 'react';
import { forwardRef } from 'react';

import { CloseIcon } from '../icon/icon';

import { cn } from '~/utils/helpers';

type HTMLBtnProps = ButtonHTMLAttributes<HTMLButtonElement>;

interface Props extends HTMLBtnProps {
    isLoading?: boolean;
}

export const Button = forwardRef<HTMLButtonElement, Props>(
    ({ isLoading, className, children, type = 'button', ...props }, ref) => (
        <button
            ref={ref}
            type={type}
            disabled={isLoading}
            className={cn(
                'btn h-10 min-h-min focus:outline-offset-0 focus:outline-secondary',
                { 'btn-disabled': props.disabled },
                className
            )}
            {...props}
        >
            {isLoading && <span className="loading loading-spinner" />}
            {children}
        </button>
    )
);

export const InputButton = forwardRef<HTMLButtonElement, HTMLBtnProps>(
    ({ className, children, type = 'button', ...props }, ref) => (
        <button
            ref={ref}
            type={type}
            className={cn(
                'input input-bordered flex h-10 items-center gap-4 hover:border-opacity-40 focus:border-primary focus:outline-1 focus:outline-offset-0 focus:outline-primary',
                className
            )}
            {...props}
        >
            {children}
        </button>
    )
);

export const IconButton = forwardRef<HTMLButtonElement, HTMLBtnProps>(
    ({ className, children, ...props }, ref) => (
        <Button
            ref={ref}
            className={cn(
                'btn-square btn-ghost w-10 text-primary focus:outline-primary',
                className
            )}
            {...props}
        >
            {children}
        </Button>
    )
);

export const CloseButton = forwardRef<HTMLButtonElement, HTMLBtnProps>(
    ({ className, ...props }, ref) => (
        <Button
            ref={ref}
            className={cn(
                'btn-square btn-ghost w-10 focus:outline-offset-0 focus:outline-primary',
                className
            )}
            {...props}
        >
            <CloseIcon className="h-4 w-4" />
        </Button>
    )
);

interface MenuBtnProps extends HTMLBtnProps {
    active?: boolean;
    hoverNoColor?: boolean;
    hoverRed?: boolean;
}

export const MenuButton = forwardRef<HTMLButtonElement, MenuBtnProps>(
    (
        {
            className,
            active,
            hoverNoColor,
            hoverRed,
            type = 'button',
            children,
            ...props
        },
        ref
    ) => (
        <button
            ref={ref}
            type={type}
            className={cn(
                'flex w-full items-center gap-2 rounded-lg px-4 py-1.5 text-left hover:bg-base-200',
                {
                    'bg-base-200 text-primary': active,
                    'hover:text-primary': !hoverNoColor,
                    'hover:text-error': hoverRed,
                },
                className
            )}
            {...props}
        >
            {children}
        </button>
    )
);
