import type { FC } from 'react';
import { Link } from '@remix-run/react';

import { _t, Alert } from '..';

export const UnexpectedError: FC = () => (
    <div>
        <Alert status="error">{_t.unexpectedError}</Alert>
        <Link className="link-primary link" to=".">
            {_t.tryAgain}
        </Link>
    </div>
);

export const UnexpectedErrorPage: FC = () => (
    <main className="mx-auto flex max-w-sm flex-col items-center justify-center gap-6 p-4">
        <Alert status="error">{_t.unexpectedError}</Alert>
        <Link className="link-primary link" to="." reloadDocument>
            {_t.tryAgain}
        </Link>
    </main>
);
