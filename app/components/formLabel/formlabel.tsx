import type { FC, LabelHTMLAttributes } from 'react';

import { Text } from '..';

import { cn } from '~/utils/helpers';

type HTMLLabelProps = LabelHTMLAttributes<HTMLLabelElement>;

interface FormLabelProps extends HTMLLabelProps {
    text: string;
    isRequired?: boolean;
}

export const FormLabel: FC<FormLabelProps> = ({
    text,
    htmlFor,
    isRequired,
}) => (
    <label className="label" htmlFor={htmlFor}>
        <Text
            className={cn('label-text text-base', {
                "after:ml-0.5 after:text-red-500 after:content-['*']":
                    isRequired,
            })}
        >
            {text}
        </Text>
    </label>
);
