import type { InputHTMLAttributes } from 'react';
import { forwardRef } from 'react';

import { FormLabel } from '..';

import { cn } from '~/utils/helpers';

interface Props extends InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    error?: string | null;
}

export const Input = forwardRef<HTMLInputElement, Props>(
    ({ className, children, error, label, ...props }, ref) => {
        const input = (
            <input
                ref={ref}
                className={cn(
                    'input input-bordered h-10 w-full bg-inherit hover:border-opacity-40 focus:border-primary focus:outline-1 focus:outline-offset-0 focus:outline-primary',
                    { 'input-error': !!error },
                    className
                )}
                {...props}
            />
        );

        return (
            <>
                {label && (
                    <FormLabel
                        text={label}
                        htmlFor={props.id}
                        isRequired={props.required}
                    />
                )}

                {children ? (
                    <div className="relative">
                        {input}
                        {children}
                    </div>
                ) : (
                    input
                )}

                {error && (
                    <label className="label">
                        <span className="label-text-alt font-semibold text-error">
                            {error}
                        </span>
                    </label>
                )}
            </>
        );
    }
);
