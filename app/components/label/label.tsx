import type { FC } from 'react';

import { CloseIcon } from '../icon/icon';
import { Text } from '..';

import type { LabelType } from '~/interfaces';
import { cn } from '~/utils/helpers';

interface BaseProps {
    onClose?: () => void;
    small?: boolean;
}

interface Props extends BaseProps {
    label: LabelType;
}

export const Label: FC<Props> = ({ onClose, label, small }) => (
    <div
        className={cn(
            'badge text-neutral-100',
            small ? 'badge-md' : 'badge-lg'
        )}
        style={{ background: `${label.color}` }}
    >
        <Text>{label.value}</Text>
        {onClose && (
            <button className="ml-2 p-0.5" onClick={onClose} aria-label="close">
                <CloseIcon className="h-3 w-3 rounded-full" />
            </button>
        )}
    </div>
);

interface BadgeProps extends BaseProps {
    value: string;
}

export const Badge: FC<BadgeProps> = ({ value, onClose, small }) => (
    <span
        className={cn(
            'badge overflow-hidden border border-primary text-primary',
            small ? 'badge-md' : 'badge-lg'
        )}
    >
        <Text className="overflow-hidden text-ellipsis">{value}</Text>
        {onClose && (
            <button className="ml-2 p-0.5" onClick={onClose} aria-label="close">
                <CloseIcon className="h-4 w-4 rounded-full opacity-60 hover:opacity-100" />
            </button>
        )}
    </span>
);
