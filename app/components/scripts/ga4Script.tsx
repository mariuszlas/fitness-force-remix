import { ENV } from '~/interfaces';
import { useConfig } from '~/providers';

export const GA4Script = () => {
    const { env, gaTrackingId } = useConfig();

    if (env !== ENV.PROD) return;

    return (
        <>
            <script
                async
                src={`https://www.googletagmanager.com/gtag/js?id=${gaTrackingId}`}
            />
            <script
                async
                id="gtag-init"
                dangerouslySetInnerHTML={{
                    __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                  
                    gtag('config', '${gaTrackingId}');
              `,
                }}
            />
        </>
    );
};
