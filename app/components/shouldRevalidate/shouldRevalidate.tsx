import type { ShouldRevalidateFunction } from '@remix-run/react';

export const defaultShouldRevalidateFn: ShouldRevalidateFunction = ({
    defaultShouldRevalidate,
    formAction,
}) => {
    if (formAction === '/set-theme') {
        return false;
    }

    return defaultShouldRevalidate;
};
