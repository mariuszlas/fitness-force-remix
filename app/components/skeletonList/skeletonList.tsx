import { cn } from '~/utils/helpers';

interface SkeletonProps {
    h?: number;
    className?: string;
}

export const Skeleton: React.FC<SkeletonProps> = ({ h, className }) => (
    <div className="w-full animate-pulse">
        <div
            className={cn(
                'rounded-lg bg-base-300',
                h ? `h-${h}` : '',
                className
            )}
        />
    </div>
);

interface SkeletonListProps {
    length: number;
}

export const SkeletonList: React.FC<SkeletonListProps> = ({ length }) => (
    <>
        {[...Array(length)].map((_, i) => (
            <Skeleton key={i} h={6} />
        ))}
    </>
);
