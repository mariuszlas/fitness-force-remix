import type { FC } from 'react';

import { MoonIcon, SunIcon } from '../icon/icon';
import { _t, IconButton, MenuButton, Text } from '..';

import { Theme, useTheme } from '~/providers';

interface ThemeSwitchProps {
    isMobile?: boolean;
}

export const ThemeSwitch: FC<ThemeSwitchProps> = ({ isMobile }) => {
    const [theme, setTheme] = useTheme();

    const isDark = (theme: Theme | null) => theme === Theme.DARK;

    const toggleTheme = () => {
        setTheme(prevTheme =>
            prevTheme === Theme.LIGHT ? Theme.DARK : Theme.LIGHT
        );
    };

    if (isMobile) {
        return (
            <li>
                <MenuButton onClick={toggleTheme} aria-label="theme-switch">
                    <Text>
                        {isDark(theme) ? _t.toggleLightMode : _t.toggleDarkMode}
                    </Text>
                    {isDark(theme) ? <SunIcon /> : <MoonIcon />}
                </MenuButton>
            </li>
        );
    }

    return (
        <IconButton
            aria-label="theme-switch"
            onClick={toggleTheme}
            className="hidden sm:inline-flex"
        >
            {isDark(theme) ? <SunIcon /> : <MoonIcon />}
        </IconButton>
    );
};
