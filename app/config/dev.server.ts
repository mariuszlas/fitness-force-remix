import shared from './shared.server';

import { ENV } from '~/interfaces';

export default {
    ...shared,
    env: ENV.TEST,
};
