import dev from './dev.server';
import { getConfig } from './helpers.server';
import prod from './prod.server';

describe('config/helpers.server', () => {
    describe('getConfig', () => {
        beforeEach(() => {
            jest.resetAllMocks();
        });

        it('should return dev config for development environment', () => {
            process.env.NODE_ENV = 'development';
            const config = getConfig();
            expect(config).toBe(dev);
        });

        it('should return prod config for production environment', () => {
            process.env.NODE_ENV = 'production';
            const config = getConfig();
            expect(config).toBe(prod);
        });
    });
});
