import axios from 'axios';

import dev from './dev.server';
import prod from './prod.server';
import type { AppConfig, Config, ToggleState } from './types';

export const getConfig = (): Config => {
    const appEnv = process.env.NODE_ENV;

    switch (appEnv) {
        case 'development':
            return dev;
        case 'production':
            return prod;
        default:
            return dev;
    }
};

export const getAppConfig = () => {
    return new Promise<AppConfig>(async resolve => {
        const config = getConfig();
        const togglesUrl = `${config.s3Url}toggles.json`;
        let togglesConfig = {};

        try {
            const response = await axios.get<ToggleState>(togglesUrl);
            if (response.status === 200) {
                togglesConfig = response.data;
            }
        } catch (_) {
            console.error(
                `Failed to fetch feature toggles from '${togglesUrl}'`
            );
        }

        resolve({ ...config, ...togglesConfig });
    });
};
