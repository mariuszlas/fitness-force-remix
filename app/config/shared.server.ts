export default {
    googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
    s3Url: process.env.S3_URL,
    gaTrackingId: process.env.GA_TRACKING_ID,
    isDemo: true,
};
