import type { ENV } from '~/interfaces';

export interface ToggleState {
    acceptNewUsers?: boolean;
}

export interface Config {
    s3Url?: string | undefined;
    googleMapsApiKey?: string | undefined;
    env?: ENV;
    gaTrackingId?: string | undefined;
    isDemo?: boolean;
}

export type AppConfig = ToggleState & Config;
