import type { Feature, LineString } from 'geojson';

export enum WorkoutTypes {
    RUNNING = 'running',
    WALKING = 'walking',
    CYCLING = 'cycling',
}

interface BaseWorkout {
    timestamp: string;
    distance: number;
    duration: number;
    type: WorkoutTypes;
    label: LabelType | null;
    notes: string | null;
    utcOffset: number;
}

export interface Workout extends BaseWorkout {
    id: number;
    pace: number;
    speed: number;
    hasTrajectory: boolean;
    trajectory: Trajectory | null;
}

export interface UploadWorkout extends BaseWorkout {
    geolocation: number[][] | null;
}

export interface PagedWorkoutList {
    content: Workout[];
    last: boolean;
    pageNo: number;
    pageSize: number;
    totalElements: number;
    totalPages: number;
}

export type Trajectory = Feature<LineString, null>;

export interface LabelType {
    value: string;
    color: string;
}

export interface TotalStats {
    counts: number;
    distance: number;
    duration: number;
    pace: number;
    speed: number;
}

export interface YearStats extends TotalStats {
    year: number;
}

export interface MonthStats extends YearStats {
    month: number;
}

export interface WorkoutsDashboard {
    total: TotalStats;
    months: MonthStats[];
    years: YearStats[];
}

export interface BestMonths {
    [key: string]: MonthStats;
}

export interface BestResults {
    [key: string]: Workout | null;
}

export interface WorkoutPreview {
    data: UploadWorkout;
    foundData: Workout[];
}

export interface User {
    username: string;
    email: string;
    lastLogin: string;
    loginCount: number;
}

export interface UserData extends User {
    name: string | undefined;
    createdAt: string | undefined;
    updatedAt: string | undefined;
}

export enum Category {
    DISTANCE = 'Distance',
    DURATION = 'Duration',
    PACE = 'Average pace',
    SPEED = 'Average speed',
    EX_TIMES = 'Exercise times',
}

export interface Login {
    accessToken: string;
    refreshToken: string;
    tokenType: string;
}

export interface UpdateWorkout {
    workout: UploadWorkout;
    id: number;
}

export interface HeaderData {
    year: number;
    secStats: number;
}

export interface CreateUserPayload {
    email: string;
    username: string;
}

export type StatIconType = 'road' | 'clockCircle' | 'speedometer' | 'counter';

export enum ENV {
    TEST = 'TEST',
    PROD = 'PROD',
}

export type PubilcRequestPayloads = CreateUserPayload;

export type PrivateRequestPayloads = UploadWorkout | UpdateWorkout;
