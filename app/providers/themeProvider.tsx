import type { Dispatch, FC, ReactNode, SetStateAction } from 'react';
import { createContext, useContext, useEffect, useRef, useState } from 'react';
import { useFetcher } from '@remix-run/react';

export enum Theme {
    LIGHT = 'light',
    DARK = 'dark',
}

const themes: Theme[] = Object.values(Theme);

export const isTheme = (value: unknown): value is Theme =>
    typeof value === 'string' && themes.includes(value as Theme);

const setDataTheme = (theme: Theme) =>
    document.querySelector('html')?.setAttribute('data-theme', theme);

type ThemeContextType = [Theme | null, Dispatch<SetStateAction<Theme | null>>];

const ThemeContext = createContext<ThemeContextType | null>(null);

type Props = {
    children: ReactNode;
    specifiedTheme: Theme | null;
};

export const ThemeProvider: FC<Props> = ({ children, specifiedTheme }) => {
    const [theme, setTheme] = useState<Theme | null>(() => {
        if (specifiedTheme) {
            if (themes.includes(specifiedTheme)) {
                return specifiedTheme;
            } else {
                return null;
            }
        }

        if (typeof window !== 'object') {
            return null;
        }

        return Theme.LIGHT;
    });

    const persistTheme = useFetcher();

    const persistThemeRef = useRef(persistTheme);
    useEffect(() => {
        persistThemeRef.current = persistTheme;
    }, [persistTheme]);

    const mountRun = useRef(false);

    useEffect(() => {
        if (!mountRun.current) {
            mountRun.current = true;
            return;
        }

        if (!theme) {
            return;
        }

        persistThemeRef.current.submit(
            { theme },
            { action: 'set-theme', method: 'post' }
        );
        setDataTheme(theme);
    }, [theme]);

    return (
        <ThemeContext.Provider value={[theme, setTheme]}>
            {children}
        </ThemeContext.Provider>
    );
};

export const useTheme = () => {
    const theme = useContext(ThemeContext);

    if (!theme) {
        throw new Error('You must use this hook inside the <ThemeProvider>');
    }

    return theme;
};
