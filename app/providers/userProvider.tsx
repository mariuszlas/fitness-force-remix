import type { FC, ReactNode } from 'react';
import { createContext, useContext } from 'react';

interface UserContext {
    username?: string | undefined;
    email?: string | undefined;
    name?: string | undefined;
    emailVerified?: boolean | undefined;
    isDemo?: boolean | undefined;
}

type Props = {
    children: ReactNode;
    userData: UserContext;
};

const UserContext = createContext<UserContext | null>(null);

export const UserProvider: FC<Props> = ({ userData, children }) => {
    return (
        <UserContext.Provider value={{ ...userData }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => {
    const user = useContext(UserContext);

    if (!user) {
        throw new Error('You must use this hook inside the <UserProvider>');
    }

    return user;
};
