import type { FC } from 'react';
import type {
    LinksFunction,
    LoaderFunctionArgs,
    MetaFunction,
} from '@remix-run/node';
import {
    Links,
    LiveReload,
    Meta,
    Outlet,
    Scripts,
    ScrollRestoration,
    useLoaderData,
} from '@remix-run/react';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/remix';

import { getAppConfig } from './config/helpers.server';
import { getThemeSession } from './server/session/themeSession.server';
import { defaultShouldRevalidateFn, GA4Script } from './components';
import { _t } from './constants';
import { ENV } from './interfaces';
import {
    ConfigProvider,
    Theme,
    ThemeProvider,
    UIProvider,
    useConfig,
    useTheme,
} from './providers';

import stylesheet from '~/main.css';

export const meta: MetaFunction = () => [
    { title: _t.pageTitle },
    { name: 'description', content: _t.landingSeoDescription },
];

export const links: LinksFunction = () => [
    ...(stylesheet ? [{ rel: 'stylesheet', href: stylesheet }] : []),
    {
        rel: 'icon',
        href: '/favicon/favicon.ico',
        type: 'image/png',
    },
    {
        rel: 'icon',
        href: '/favicon/favicon-32x32.png',
        type: 'image/png',
        sizes: '32x32',
    },
    {
        rel: 'icon',
        href: '/favicon/favicon-16x16.png',
        type: 'image/png',
        sizes: '16x16',
    },
    {
        rel: 'manifest',
        href: '/favicon/site.webmanifest',
    },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
    const themeSession = await getThemeSession(request);
    const appConfig = await getAppConfig();
    return { appConfig, theme: themeSession.getTheme() };
};

const App: FC = () => {
    const [theme] = useTheme();
    const { s3Url, env } = useConfig();

    return (
        <html
            lang="en"
            className="transition-colors duration-150"
            data-theme={theme}
            suppressHydrationWarning
        >
            <head>
                <meta charSet="utf-8" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <meta
                    name="color-scheme"
                    content={theme === Theme.DARK ? 'dark light' : 'light dark'}
                />
                <meta property="og:image" content={`${s3Url}og-image.webp`} />
                <meta property="og:title" content={_t.pageTitle} />
                <meta
                    property="og:description"
                    content={_t.landingSeoDescription}
                />
                <meta
                    property="og:url"
                    content="https://www.workoutodyssey.com"
                />
                <Meta />
                <Links />
                <GA4Script />
            </head>
            <body>
                <Outlet />
                <ScrollRestoration />
                <Scripts />
                <LiveReload />
                {env === ENV.PROD && (
                    <>
                        <Analytics />
                        <SpeedInsights />
                    </>
                )}
            </body>
        </html>
    );
};

export default function AppWithProviders() {
    const { appConfig, theme } = useLoaderData<typeof loader>();

    return (
        <ConfigProvider appConfig={appConfig}>
            <UIProvider>
                <ThemeProvider specifiedTheme={theme}>
                    <App />
                </ThemeProvider>
            </UIProvider>
        </ConfigProvider>
    );
}

export const shouldRevalidate = defaultShouldRevalidateFn;
