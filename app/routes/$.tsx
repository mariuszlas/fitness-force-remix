import type { MetaFunction } from '@remix-run/node';
import { Link } from '@remix-run/react';

import { Heading, Text } from '~/components';

export const meta: MetaFunction = () => [
    { title: 'Not Found' },
    { name: 'description', content: '404 Page Not Found' },
];

export const loader = () => {
    throw new Response(null, {
        status: 404,
        statusText: 'Page Not Found',
    });
};

export const ErrorBoundary = () => {
    return (
        <main className="flex h-[50vh] flex-col items-center justify-center gap-6 p-4">
            <Heading>Page Not Found</Heading>
            <Text as="p">There's nothing here</Text>
            <Text as="p">
                Back to{' '}
                <Link className="link-primary link" to="/">
                    Home
                </Link>
            </Text>
        </main>
    );
};
