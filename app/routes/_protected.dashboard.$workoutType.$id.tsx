import {
    json,
    type LoaderFunctionArgs,
    type MetaFunction,
} from '@remix-run/node';
import { useLoaderData, useLocation, useNavigate } from '@remix-run/react';

import { _t, Drawer, ModalHeader, UnexpectedError } from '~/components';
import type { Trajectory } from '~/interfaces';
import { Api } from '~/server/api/api.server';
import { handleError } from '~/server/helpers.server';
import { Map, WorkoutDetailsPanel } from '~/views';

export const meta: MetaFunction = () => [
    { title: 'Workout' },
    { name: 'description', content: 'View details of your workout' },
];

export const loader = async ({ request, params }: LoaderFunctionArgs) => {
    try {
        const api = new Api();
        await api.setToken(request);
        return json({ workout: await api.getWorkoutByIdGeo(params?.id) });
    } catch (e) {
        return handleError(e, request);
    }
};

export default function WorkoutDetails() {
    const { workout } = useLoaderData<typeof loader>();
    const navigate = useNavigate();
    const location = useLocation();

    return (
        <Drawer isOpen={true} size={location.state?.isLarge ? 'lg' : 'sm'}>
            <div className="flex h-full flex-col">
                <ModalHeader className="p-4" onClose={() => navigate(-1)}>
                    {_t.workoutDetails}
                </ModalHeader>

                <hr className="border-t border-t-base-content border-opacity-20 " />

                <div className="flex-grow p-4 sm:p-6">
                    <div className="flex h-full flex-col gap-4">
                        <WorkoutDetailsPanel data={workout} />

                        {workout?.trajectory && (
                            <Map
                                trajectory={workout.trajectory as Trajectory}
                            />
                        )}
                    </div>
                </div>
            </div>
        </Drawer>
    );
}

export const ErrorBoundary = UnexpectedError;
