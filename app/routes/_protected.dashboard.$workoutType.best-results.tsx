import {
    json,
    type LoaderFunctionArgs,
    type MetaFunction,
} from '@remix-run/node';
import { useLoaderData, useMatches, useNavigate } from '@remix-run/react';

import { Button, Modal, ModalHeader } from '~/components';
import type { WorkoutTypes } from '~/interfaces';
import { Api } from '~/server/api/api.server';
import { handleError } from '~/server/helpers.server';
import { _t, BestResults } from '~/views';

export const meta: MetaFunction = () => [
    { title: 'Best Results' },
    { name: 'description', content: 'View your best results' },
];

export async function loader({ request, params }: LoaderFunctionArgs) {
    try {
        const api = new Api(params);
        await api.setToken(request);
        return json({ data: await api.getBestResults() });
    } catch (e) {
        return handleError(e, request);
    }
}

export default function BestResultsModal() {
    const { data } = useLoaderData<typeof loader>();
    const navigate = useNavigate();
    const matches = useMatches();

    const workoutType = matches.at(2)?.params?.workoutType as WorkoutTypes;

    return (
        <Modal isOpen={true} full>
            <ModalHeader onClose={() => navigate(-1)}>
                {`${_t.bestResults} for ${workoutType}`}
            </ModalHeader>

            <BestResults data={data} workoutType={workoutType} />

            <footer className="mt-6 flex justify-end">
                <Button className="btn-primary" onClick={() => navigate(-1)}>
                    {_t.btnClose}
                </Button>
            </footer>
        </Modal>
    );
}

export const ErrorBoundary = () => <div>{_t.errorFetchBestResults}</div>;
