import { useEffect } from 'react';
import type { ActionFunctionArgs, MetaFunction } from '@remix-run/node';
import { json } from '@remix-run/node';
import {
    Form,
    useActionData,
    useNavigate,
    useNavigation,
} from '@remix-run/react';

import { _t, Button, Modal, ModalHeader, Text } from '~/components';
import { Api } from '~/server/api/api.server';
import { handleError } from '~/server/helpers.server';

export const meta: MetaFunction = () => [
    { title: 'Delete workout' },
    { name: 'description', content: 'Delete workout data' },
];

export default function DeleteWorkout() {
    const navigate = useNavigate();
    const navigation = useNavigation();
    const data = useActionData<typeof action>();

    const isLoading =
        navigation.formMethod === 'DELETE' &&
        (navigation.state === 'loading' || navigation.state === 'submitting');

    useEffect(() => {
        if (data?.ok) {
            navigate('..', { state: { success: true } });
        }

        if (data?.ok === false) {
            navigate('..', { state: { success: false } });
        }
    }, [data, navigate]);

    return (
        <Modal isOpen={true}>
            <div className="flex flex-col gap-4">
                <ModalHeader onClose={() => navigate(-1)}>
                    {_t.dataDeletionHeader}
                </ModalHeader>

                <Text as="p">{_t.deleteConfirmation}</Text>

                <div className="flex justify-end gap-4">
                    <Button onClick={() => navigate(-1)} className="btn-ghost">
                        {_t.btnCancel}
                    </Button>
                    <Form method="DELETE">
                        <Button
                            type="submit"
                            className="btn-error"
                            isLoading={isLoading}
                        >
                            {_t.delete}
                        </Button>
                    </Form>
                </div>
            </div>
        </Modal>
    );
}

export const action = async ({ request, params }: ActionFunctionArgs) => {
    try {
        const api = new Api();
        await api.setToken(request);

        const response = await api.deleteWorkout(params?.id);

        if (response === 'OK') {
            return json({ ok: true });
        }

        return json({ ok: false });
    } catch (e) {
        return handleError(e, request);
    }
};
