import type {
    ActionFunctionArgs,
    LoaderFunctionArgs,
    MetaFunction,
} from '@remix-run/node';
import { json } from '@remix-run/node';
import { useLoaderData } from '@remix-run/react';

import { Api } from '~/server/api/api.server';
import { uploadWorkoutData } from '~/server/dataUpload.server';
import { handleError } from '~/server/helpers.server';
import { DataUploadView } from '~/views/dataUpload/dataUpload';

export const meta: MetaFunction = () => [
    { title: 'Edit workout' },
    { name: 'description', content: 'Edit existing workout data' },
];

export const loader = async ({ request, params }: LoaderFunctionArgs) => {
    try {
        const api = new Api(params);
        await api.setToken(request);
        return json({
            workout: await api.getWorkoutById(params?.id),
            labels: await api.getAllLabels(),
        });
    } catch (e) {
        return handleError(e, request);
    }
};

export default function EditWorkout() {
    const { workout, labels } = useLoaderData<typeof loader>();
    return <DataUploadView workout={workout} labels={labels} />;
}

export const action = async (requestParams: ActionFunctionArgs) => {
    return await uploadWorkoutData(requestParams, true);
};
