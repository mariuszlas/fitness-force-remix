import type {
    ActionFunctionArgs,
    LoaderFunctionArgs,
    MetaFunction,
} from '@remix-run/node';
import { json } from '@remix-run/node';
import { useLoaderData } from '@remix-run/react';

import { Api } from '~/server/api/api.server';
import { uploadWorkoutData } from '~/server/dataUpload.server';
import { handleError } from '~/server/helpers.server';
import { DataUploadView } from '~/views/dataUpload/dataUpload';

export const meta: MetaFunction = () => [
    { title: 'New workout' },
    { name: 'description', content: 'Add new workout data' },
];

export const loader = async ({ request }: LoaderFunctionArgs) => {
    try {
        const api = new Api();
        await api.setToken(request);
        return json({ labels: await api.getAllLabels() });
    } catch (e) {
        return handleError(e, request);
    }
};

export default function AddNewWorkout() {
    const { labels } = useLoaderData<typeof loader>();
    return <DataUploadView labels={labels} />;
}

export const action = async (requestParams: ActionFunctionArgs) => {
    return await uploadWorkoutData(requestParams, false);
};
