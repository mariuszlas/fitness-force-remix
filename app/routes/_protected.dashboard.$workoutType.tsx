import type { LoaderFunctionArgs, MetaFunction } from '@remix-run/node';
import { json } from '@remix-run/node';
import { Outlet, useLoaderData } from '@remix-run/react';

import { defaultShouldRevalidateFn, UnexpectedErrorPage } from '~/components';
import { Api } from '~/server/api/api.server';
import { handleError } from '~/server/helpers.server';
import { _t, ChartView, StatisticsView, WorkoutListView } from '~/views';

export const meta: MetaFunction = () => [
    { title: 'Dashboard' },
    { name: 'description', content: 'View your workouts data' },
];

export async function loader({ request, params }: LoaderFunctionArgs) {
    try {
        const api = new Api(params);
        await api.setToken(request);

        return json({
            dashboard: await api.getDashboard(),
            workoutList: await api.getWorkoutsList(),
        });
    } catch (e) {
        return handleError(e, request);
    }
}

export default function Dashboard() {
    const { dashboard, workoutList } = useLoaderData<typeof loader>();

    return (
        <>
            <main className="flex justify-center align-middle">
                <div className="grid w-full max-w-8xl grid-cols-12 sm:gap-4 sm:px-8 sm:py-4">
                    <div className="col-span-12 lg:col-span-6 xl:col-span-7">
                        <ChartView dashboard={dashboard} />
                        <StatisticsView dashboard={dashboard} />
                    </div>

                    <div className="col-span-12 lg:col-span-6 xl:col-span-5">
                        <WorkoutListView data={workoutList} />
                    </div>
                </div>
            </main>

            <Outlet />
        </>
    );
}

export const ErrorBoundary = UnexpectedErrorPage;

export const shouldRevalidate = defaultShouldRevalidateFn;
