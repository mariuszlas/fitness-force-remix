import {
    json,
    type LoaderFunctionArgs,
    type MetaFunction,
} from '@remix-run/node';
import { useLoaderData, useNavigate } from '@remix-run/react';

import { _t, Button, Modal, ModalHeader } from '~/components';
import type { UserData } from '~/interfaces';
import { useUser } from '~/providers';
import { Api } from '~/server/api/api.server';
import { CognitoApi } from '~/server/api/cognitoAuth.server';
import { handleError } from '~/server/helpers.server';
import { getSessionEmail } from '~/server/session/authSession.server';
import { AccountSettings } from '~/views';

export const meta: MetaFunction = () => [
    { title: 'User details' },
    { name: 'description', content: 'View your user details' },
];

export async function loader({ request }: LoaderFunctionArgs) {
    try {
        const api = new Api();
        await api.setToken(request);
        const user = await api.getUser();

        const email = await getSessionEmail(request);
        const cognitoUser = await new CognitoApi().getUser(email);

        return json({
            ...user,
            createdAt: cognitoUser.UserCreateDate,
            updatedAt: cognitoUser.UserLastModifiedDate,
        });
    } catch (e) {
        return handleError(e, request);
    }
}

export default function AccountDetails() {
    const user = useLoaderData<typeof loader>();
    const { name } = useUser();
    const navigate = useNavigate();

    return (
        <Modal isOpen={true} full>
            <ModalHeader onClose={() => navigate(-1)}>
                {_t.accountSettings}
            </ModalHeader>

            <AccountSettings user={{ ...user, name } as UserData} />

            <footer className="mt-6 flex justify-end">
                <Button className="btn-primary" onClick={() => navigate(-1)}>
                    {_t.btnClose}
                </Button>
            </footer>
        </Modal>
    );
}

export const ErrorBoundary = () => <div>{_t.errorFetchUserData}</div>;
