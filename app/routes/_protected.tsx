import type { LoaderFunctionArgs } from '@remix-run/node';
import { json, redirect } from '@remix-run/node';
import { Outlet, useLoaderData } from '@remix-run/react';

import { defaultShouldRevalidateFn } from '~/components';
import { UserProvider } from '~/providers';
import {
    getSessionUser,
    requireUserSession,
} from '~/server/session/authSession.server';
import { NavBar } from '~/views';

export async function loader({ request }: LoaderFunctionArgs) {
    try {
        const session = await requireUserSession(request);
        const user = getSessionUser(session);
        return json(user);
    } catch (_) {
        return redirect('/login');
    }
}

export default function ProtectedLayout() {
    const cognitoUser = useLoaderData<typeof loader>();

    return (
        <UserProvider userData={cognitoUser}>
            <div id="page-content" className="flex min-h-screen flex-col">
                <NavBar isProtected={true} />
                <Outlet />
            </div>
        </UserProvider>
    );
}

export const shouldRevalidate = defaultShouldRevalidateFn;
