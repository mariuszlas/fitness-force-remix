import { useState } from 'react';
import type {
    ActionFunctionArgs,
    LoaderFunctionArgs,
    MetaFunction,
} from '@remix-run/node';
import { json, redirect } from '@remix-run/node';
import { Form, Link, useActionData, useNavigation } from '@remix-run/react';

import {
    _t,
    Alert,
    Button,
    EyeIcon,
    EyeSlashIcon,
    IconButton,
    Input,
    UnexpectedError,
} from '~/components';
import { loginUser } from '~/server/login.server';
import { getSession } from '~/server/session/authSession.server';

export const meta: MetaFunction = () => [
    { title: _t.loginHeader },
    { name: 'description', content: 'Log in to access your data' },
];

export async function loader({ request }: LoaderFunctionArgs) {
    const session = await getSession(request);

    if (session.has('idToken')) {
        return redirect('/dashboard/running');
    }

    return json({});
}

export default function Login() {
    const navigation = useNavigation();
    const [showPassword, setShowPassword] = useState(false);

    const actionData = useActionData<typeof action>();
    const otherError = actionData?.errors?.other;
    const emailError = actionData?.errors?.email;

    return (
        <Form method="POST" className="form-control w-full gap-6">
            <div className="form-control gap-2">
                {otherError && (
                    <Alert status="error" classes="mb-0">
                        {otherError}
                    </Alert>
                )}

                <div>
                    <Input
                        autoFocus
                        required
                        id="email"
                        name="email"
                        type="email"
                        label={_t.labelEmail}
                        placeholder={_t.plcdEmail}
                        error={emailError ?? undefined}
                        aria-invalid={Boolean(emailError)}
                        aria-errormessage={emailError ?? undefined}
                    />
                </div>

                <div>
                    <Input
                        required
                        id="password"
                        name="password"
                        type={showPassword ? 'text' : 'password'}
                        className="pr-10"
                        label={_t.labelPass}
                        placeholder={_t.plcdPassword}
                    >
                        <IconButton
                            onClick={() => setShowPassword(prev => !prev)}
                            className="absolute right-0"
                            aria-label="toggle password visibility"
                        >
                            {showPassword ? <EyeSlashIcon /> : <EyeIcon />}
                        </IconButton>
                    </Input>

                    <div className="label justify-end">
                        <Link
                            className="link-primary link"
                            to="/password-reset"
                        >
                            {_t.forgotPass}
                        </Link>
                    </div>
                </div>
            </div>

            <Button
                type="submit"
                isLoading={navigation.state === 'submitting'}
                className="btn-primary btn-block"
            >
                {_t.btnLogin}
            </Button>

            <div>
                <span>{_t.signupOffer}</span>
                <Link to="/signup" className="link-primary link font-medium">
                    {_t.signupHeader}
                </Link>
            </div>
        </Form>
    );
}

export async function action({ request }: ActionFunctionArgs) {
    return await loginUser(request);
}

export const ErrorBoundary = UnexpectedError;
