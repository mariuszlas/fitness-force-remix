import {
    CodeMismatchException,
    ExpiredCodeException,
} from '@aws-sdk/client-cognito-identity-provider';
import type { ActionFunctionArgs, MetaFunction } from '@remix-run/node';
import { redirect } from '@remix-run/node';
import { useActionData, useFetcher } from '@remix-run/react';

import { Alert, Button, Input, Text } from '~/components';
import { _t } from '~/constants';
import { CognitoApi } from '~/server/api/cognitoAuth.server';
import type { errs } from '~/server/validation.server';
import {
    handleOtherError,
    handleValidationError,
    validateEmail,
    validatePassword,
    ValidationError,
} from '~/server/validation.server';
import { ResetPasswordForm } from '~/views';

export const meta: MetaFunction = () => [
    { title: _t.passwordResetHeader },
    { name: 'description', content: 'Reset your password' },
];

export default function PasswordReset() {
    const requestPasswordReset = useFetcher<{
        ok: boolean;
        email: string;
        errors: errs;
    }>();

    const requestPasswordResetOtherError =
        requestPasswordReset.data?.errors?.other;
    const requestPasswordResetEmailError =
        requestPasswordReset.data?.errors?.email;
    const requestPasswordResetSuccess = requestPasswordReset.data?.ok;

    const actionData = useActionData<typeof action>();

    return (
        <>
            {requestPasswordResetSuccess ? (
                <ResetPasswordForm
                    email={requestPasswordReset.data?.email}
                    actionData={actionData}
                />
            ) : (
                <requestPasswordReset.Form
                    method="POST"
                    action="/request-password-reset"
                    className="form-control w-full gap-6"
                >
                    {requestPasswordResetOtherError && (
                        <Alert status="error" classes="mb-0">
                            {requestPasswordResetOtherError}
                        </Alert>
                    )}

                    <Text as="p">{_t.passwordResetBody1}</Text>

                    <div>
                        <Input
                            autoFocus
                            required
                            id="email"
                            name="email"
                            type="email"
                            placeholder={_t.plcdEmail}
                            error={requestPasswordResetEmailError ?? undefined}
                            aria-invalid={Boolean(
                                requestPasswordResetEmailError
                            )}
                            aria-errormessage={
                                requestPasswordResetEmailError ?? undefined
                            }
                        />
                    </div>

                    <Button
                        type="submit"
                        isLoading={
                            requestPasswordReset.state === 'submitting' ||
                            requestPasswordReset.state === 'loading'
                        }
                        className="btn-primary btn-block"
                    >
                        {_t.passwordResetBtn}
                    </Button>
                </requestPasswordReset.Form>
            )}
        </>
    );
}

export async function action({ request }: ActionFunctionArgs) {
    const formData = await request.formData();
    const email = formData.get('email')?.toString() ?? '';
    const passwordResetCode =
        formData.get('passwordResetCode')?.toString() ?? '';
    const password = formData.get('password')?.toString() ?? '';
    const passwordRepeat = formData.get('passwordRepeat')?.toString() ?? '';

    try {
        validateEmail(email);
        validatePassword(password, passwordRepeat);

        const response = await new CognitoApi().resetPassword(
            email,
            password,
            passwordResetCode
        );

        if (response.$metadata.httpStatusCode === 200) {
            return redirect('/login');
        }

        throw new Error('Error occured while trying to reset password');
    } catch (e) {
        if (e instanceof ValidationError) {
            return handleValidationError(e);
        }

        if (
            e instanceof CodeMismatchException ||
            e instanceof ExpiredCodeException
        ) {
            return handleOtherError(e.message);
        }

        const msg = 'Failed to reset password';
        console.error(msg);
        return handleOtherError(msg, false);
    }
}
