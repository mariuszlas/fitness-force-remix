import { useState } from 'react';
import { type ActionFunctionArgs, type MetaFunction } from '@remix-run/node';
import { Form, Link, useActionData, useNavigation } from '@remix-run/react';

import {
    _t,
    Alert,
    Button,
    IconButton,
    Input,
    UnexpectedError,
} from '~/components';
import { EyeIcon, EyeSlashIcon } from '~/components/icon/icon';
import { getAppConfig } from '~/config/helpers.server';
import { signupUser } from '~/server/signup.server';

export const meta: MetaFunction = () => [
    { title: _t.signupHeader },
    { name: 'description', content: _t.signupSubH },
];

export default function Signup() {
    const navigation = useNavigation();
    const [showPassword, setShowPassword] = useState(false);
    const [showPasswordRepeat, setShowPasswordRepeat] = useState(false);

    const actionData = useActionData<typeof action>();
    const otherError = actionData?.errors?.other;
    const emailError = actionData?.errors?.email;
    const nameError = actionData?.errors?.name;
    const passwordError = actionData?.errors?.password;

    return (
        <Form method="POST" className="form-control w-full gap-6">
            <div className="form-control gap-2">
                {otherError && (
                    <Alert status="error" classes="mb-0">
                        {otherError}
                    </Alert>
                )}

                <div>
                    <Input
                        autoFocus
                        required
                        id="email"
                        name="email"
                        type="email"
                        label={_t.labelEmail}
                        placeholder={_t.plcdEmail}
                        error={emailError ?? undefined}
                        aria-invalid={Boolean(emailError)}
                        aria-errormessage={emailError ?? undefined}
                    />
                </div>

                <div>
                    <Input
                        required
                        id="name"
                        name="name"
                        type="text"
                        label={_t.labelName}
                        placeholder={_t.plcdName}
                        error={nameError ?? undefined}
                        aria-invalid={Boolean(nameError)}
                        aria-errormessage={nameError ?? undefined}
                    />
                </div>

                <div>
                    <Input
                        required
                        id="password"
                        name="password"
                        type={showPassword ? 'text' : 'password'}
                        className={'pr-10'}
                        label={_t.labelPass}
                        placeholder={_t.plcdPassword}
                        error={passwordError ?? undefined}
                        aria-invalid={Boolean(passwordError)}
                        aria-errormessage={passwordError ?? undefined}
                    >
                        <IconButton
                            onClick={() => setShowPassword(prev => !prev)}
                            className="absolute right-0"
                            aria-label="toggle password visibility"
                        >
                            {showPassword ? <EyeSlashIcon /> : <EyeIcon />}
                        </IconButton>
                    </Input>
                </div>

                <div>
                    <Input
                        required
                        id="passwordRepeat"
                        name="passwordRepeat"
                        type={showPasswordRepeat ? 'text' : 'password'}
                        className={'pr-10'}
                        label={_t.labelPassRep}
                        placeholder={_t.plcdPassword}
                        error={passwordError ?? undefined}
                        aria-invalid={Boolean(passwordError)}
                        aria-errormessage={passwordError ?? undefined}
                    >
                        <IconButton
                            onClick={() => setShowPasswordRepeat(prev => !prev)}
                            className="absolute right-0"
                            aria-label="toggle password repeat visibility"
                        >
                            {showPasswordRepeat ? (
                                <EyeSlashIcon />
                            ) : (
                                <EyeIcon />
                            )}
                        </IconButton>
                    </Input>
                </div>
            </div>

            <Button
                type="submit"
                isLoading={navigation.state === 'submitting'}
                className="btn-primary btn-block"
            >
                {_t.btnSignup}
            </Button>

            <div>
                <span>{_t.loginOffer}</span>
                <Link to="/login" className="link-primary link font-medium">
                    {_t.loginHeader}
                </Link>
            </div>
        </Form>
    );
}

export async function action({ request }: ActionFunctionArgs) {
    const { acceptNewUsers, isDemo } = await getAppConfig();
    return await signupUser(request, acceptNewUsers, isDemo);
}

export const ErrorBoundary = UnexpectedError;
