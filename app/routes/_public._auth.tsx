import { Outlet, useMatches } from '@remix-run/react';

import { _t } from '../constants';

import { Heading } from '~/components';

export default function AuthLayout() {
    const matches = useMatches();
    const formType = matches.at(3)?.pathname?.split('/')?.at(1);

    const getHeader = (formType: string | undefined) => {
        switch (formType) {
            case 'login':
                return _t.loginHeader;
            case 'signup':
                return _t.signupHeader;
            case 'verify':
                return _t.emailConfirmationHeader;
            case 'password-reset':
                return _t.passwordResetHeader;
            default:
                return _t.loginHeader;
        }
    };

    return (
        <main className="flex grow sm:bg-base-200">
            <section
                className="sm:border-1 m-auto w-full max-w-md bg-base-100 p-8 sm:rounded-xl sm:shadow-lg"
                data-testid={`${formType}-form`}
            >
                <header className="pb-4">
                    <Heading>{getHeader(formType)}</Heading>
                </header>

                <Outlet />
            </section>
        </main>
    );
}
