import { UserNotFoundException } from '@aws-sdk/client-cognito-identity-provider';
import type {
    ActionFunctionArgs,
    LoaderFunctionArgs,
    MetaFunction,
} from '@remix-run/node';
import { json, redirect } from '@remix-run/node';
import {
    Form,
    Link,
    useActionData,
    useNavigation,
    useSearchParams,
} from '@remix-run/react';

import { Alert, Button, CheckIcon, Text } from '~/components';
import { _t } from '~/constants';
import { CognitoApi } from '~/server/api/cognitoAuth.server';

export const meta: MetaFunction = () => [
    { title: _t.emailConfirmationHeader },
    { name: 'description', content: 'Verify your email' },
];

export async function loader({ request }: LoaderFunctionArgs) {
    const { searchParams } = new URL(request.url);
    const email = searchParams.get('user');

    if (!email) {
        return redirect('/');
    }

    try {
        const response = await new CognitoApi().getUser(email);

        const isEmailVerified = response.UserAttributes?.find(
            ({ Name, Value }) => Name === 'email_verified' && Value === 'true'
        );
        const isUnconfirmed = response.UserStatus === 'UNCONFIRMED';

        if (!isEmailVerified && isUnconfirmed) {
            return json({});
        }

        return redirect('/login');
    } catch (e) {
        if (e instanceof UserNotFoundException) {
            console.error('Error: Verfication failed - user not found');
            redirect('/signup');
        }

        console.error('Error: Verfication failed');
        return redirect('/');
    }
}

export default function Verify() {
    const navigation = useNavigation();
    const [urlParams] = useSearchParams();
    const email = urlParams.get('user') ?? '';

    const actionData = useActionData<typeof action>();
    const success = actionData?.success;
    const error = actionData?.error;

    return (
        <div className="flex flex-col gap-8">
            {error && (
                <Alert status="error" classes="mb-0">
                    {error}
                </Alert>
            )}

            <Text as="p">
                {_t.emailConfirmationBody1}
                <Text className="font-bold">{email}</Text>
                {_t.emailConfirmationBody2}
            </Text>

            <div className="flex flex-wrap justify-end gap-4">
                <div className="flex items-center gap-2">
                    {navigation.state === 'submitting' ? (
                        <span className="loading loading-spinner" />
                    ) : (
                        success && <CheckIcon className="text-success" />
                    )}

                    <Form method="POST" replace>
                        <input
                            name="email"
                            value={email}
                            type="text"
                            className="hidden"
                            readOnly
                        />
                        <Button
                            type="submit"
                            className="btn btn-primary btn-outline"
                            disabled={navigation.state === 'submitting'}
                        >
                            {_t.resendCodeCTA}
                        </Button>
                    </Form>
                </div>

                <Link
                    className="btn btn-primary  h-10 min-h-min"
                    to="/login"
                    color="primary"
                    replace
                >
                    {_t.btnLogin}
                </Link>
            </div>
        </div>
    );
}

export async function action({ request }: ActionFunctionArgs) {
    const formData = await request.formData();
    const email = formData.get('email')?.toString() ?? '';

    try {
        const response = await new CognitoApi().resendConfirmationCode(email);

        if (response.$metadata.httpStatusCode === 200) {
            return json({ success: true, error: null });
        }

        throw new Error();
    } catch (e) {
        if (e instanceof UserNotFoundException) {
            console.error('Error: Verfication failed - user not found');
            redirect('/signup');
        }

        const msg = 'Failed to resend the verification email';
        console.error(msg);
        return json({ success: false, error: msg });
    }
}
