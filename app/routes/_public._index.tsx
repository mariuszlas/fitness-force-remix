import { Link } from '@remix-run/react';

import { _t, Heading, Text } from '~/components';
import { Theme, useConfig, useTheme } from '~/providers';
import { cn } from '~/utils/helpers';

export default function Index() {
    const [theme] = useTheme();
    const { s3Url } = useConfig();

    const images = [
        {
            img: `chart-${theme || Theme.LIGHT}.webp`,
            title: _t.landing1Title,
            desc: _t.landing1Desc,
            w: 750,
            eager: true,
        },
        {
            img: `stats-${theme || Theme.LIGHT}.webp`,
            title: _t.landing2Title,
            desc: _t.landing2Desc,
            w: 750,
        },
        {
            img: `list-${theme || Theme.LIGHT}.webp`,
            title: _t.landing3Title,
            desc: _t.landing3Desc,
            w: 600,
        },
        {
            img: `best-results-${theme || Theme.LIGHT}.webp`,
            title: _t.landing4Title,
            desc: _t.landing4Desc,
            w: 450,
        },
    ];

    return (
        <main className="flex flex-col items-center gap-10 p-4 sm:p-10 lg:gap-20">
            <Heading className="md:text-4xl">{_t.landingHeader}</Heading>

            <Link
                role="button"
                to="/dashboard/running"
                className="btn btn-primary btn-lg h-12 min-h-full"
            >
                {_t.btnGetStarted}
            </Link>

            {images.map(({ img, title, desc, w, eager }, idx) => (
                <section
                    key={title}
                    className={cn(
                        'flex flex-col gap-6 lg:gap-10',
                        idx % 2 === 0 ? 'lg:flex-row' : 'lg:flex-row-reverse'
                    )}
                >
                    <header className="flex flex-col gap-1 lg:max-w-xs lg:gap-3">
                        <Heading className="md:text-3xl" as="h2">
                            {title}
                        </Heading>
                        <Text className="text-2xl">{desc}</Text>
                    </header>

                    <img
                        src={`${s3Url}img/${img}`}
                        width={w}
                        alt="app screenshot"
                        loading={eager ? 'eager' : 'lazy'}
                    />
                </section>
            ))}
        </main>
    );
}
