import { Outlet } from '@remix-run/react';

import { Footer, NavBar } from '~/views';

export default function PublicLayout() {
    return (
        <div id="page-content" className="flex min-h-screen flex-col">
            <NavBar isProtected={false} />
            <Outlet />
            <Footer />
        </div>
    );
}
