import { UserNotFoundException } from '@aws-sdk/client-cognito-identity-provider';
import type { ActionFunctionArgs } from '@remix-run/node';
import { json, redirect } from '@remix-run/node';

import { CognitoApi } from '~/server/api/cognitoAuth.server';
import {
    handleOtherError,
    handleValidationError,
    validateEmail,
    ValidationError,
} from '~/server/validation.server';

export async function action({ request }: ActionFunctionArgs) {
    const formData = await request.formData();
    const email = formData.get('email')?.toString() ?? '';

    try {
        validateEmail(email);
        const response = await new CognitoApi().requestPasswordReset(email);

        if (response.$metadata.httpStatusCode === 200) {
            redirect('/password-reset?email');
            return json({ ok: true, email });
        }

        throw new Error();
    } catch (e) {
        if (e instanceof ValidationError) {
            return handleValidationError(e);
        }

        if (e instanceof UserNotFoundException) {
            console.error('Error: Password reset failed - user not found');
            return handleOtherError(
                'User with that email does not exist',
                false
            );
        }

        const msg = 'Failed to resend the password reset code';
        return handleOtherError(msg);
    }
}

export const loader = () => redirect('/');
