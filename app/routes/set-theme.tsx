import type { ActionFunctionArgs } from '@remix-run/node';
import { json, redirect } from '@remix-run/node';

import { isTheme } from '~/providers';
import {
    getThemeFromRequest,
    getThemeSession,
} from '~/server/session/themeSession.server';

export const action = async ({ request }: ActionFunctionArgs) => {
    const themeSession = await getThemeSession(request);
    const theme = await getThemeFromRequest(request);

    if (!isTheme(theme)) {
        return json({
            success: false,
            message: `'${theme}' is not a valid theme`,
        });
    }

    themeSession.setTheme(theme);

    return json(
        { success: true },
        { headers: { 'Set-Cookie': await themeSession.commit() } }
    );
};

export const loader = () => redirect('/');
