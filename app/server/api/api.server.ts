import type { Params } from '@remix-run/react';
import axios from 'axios';

import type {
    BestResults,
    CreateUserPayload,
    LabelType,
    PagedWorkoutList,
    PrivateRequestPayloads,
    PubilcRequestPayloads,
    UpdateWorkout,
    UploadWorkout,
    User,
    Workout,
    WorkoutPreview,
    WorkoutsDashboard,
} from '~/interfaces';
import type { AuthSession } from '~/server/session/authSession.server';
import {
    getSessionTokens,
    logout,
    requireUserSession,
} from '~/server/session/authSession.server';

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';

export class Api {
    #baseURL: string;
    #idToken: string | undefined;
    #refreshToken: string | undefined;
    #session: AuthSession | undefined;
    #params: Params<string> | undefined;

    constructor(params?: Params<string>) {
        const baseURL = process.env.API_URL;

        if (!baseURL) throw new Error('API URL was not provided');

        this.#baseURL = baseURL;
        if (params) this.#params = params;
    }

    async setToken(request: Request) {
        const session = await requireUserSession(request);
        const { idToken, refreshToken } = getSessionTokens(session);

        if (!idToken || !refreshToken) {
            throw await logout(request);
        }

        this.#session = session;
        this.setTokens(idToken, refreshToken);
    }

    setTokens(idToken: string, refreshToken: string) {
        this.#idToken = idToken;
        this.#refreshToken = refreshToken;
    }

    #createInstance = () =>
        axios.create({ baseURL: this.#baseURL, withCredentials: false });

    #initializeInstance = () => {
        const instance = this.#createInstance();

        instance.interceptors.request.use(
            config => {
                return config;
            },
            error => {
                return Promise.reject(error);
            }
        );

        return instance;
    };

    #initializeAuthInstance = () => {
        const instance = this.#createInstance();

        instance.interceptors.request.use(
            config => {
                config.headers.set('Authorization', `Bearer ${this.#idToken}`);
                return config;
            },
            error => {
                return Promise.reject(error);
            }
        );

        return instance;
    };

    #publicRequest = async <T>(
        url: string,
        method: string,
        data: PubilcRequestPayloads
    ) => {
        const instance = this.#initializeInstance();

        const result = await instance<T>({
            url,
            method,
            data,
        });
        return result.data;
    };

    #authRequest = async <T>(
        url: string,
        method: string = GET,
        data?: PrivateRequestPayloads
    ) => {
        const instance = this.#initializeAuthInstance();

        const result = await instance<T>({
            url,
            method,
            data,
        });
        return result.data;
    };

    createUser = (payload: CreateUserPayload) =>
        this.#publicRequest<User>('/users', POST, payload);

    reportUserLogin = () =>
        this.#authRequest<string>('/users/current/report-login');

    getDashboard = () =>
        this.#authRequest<WorkoutsDashboard>(
            `/workouts/dashboard/${this.#params?.workoutType}`
        );

    getWorkoutsList = () =>
        this.#authRequest<PagedWorkoutList>(
            `/workouts/list/${this.#params?.workoutType}`
        );

    getBestResults = () =>
        this.#authRequest<BestResults>(
            `/workouts/records/${this.#params?.workoutType}`
        );

    getWorkoutById = (id: string | undefined) =>
        this.#authRequest<Workout>(`/workouts/${id}`);

    getWorkoutByIdGeo = (id: string | undefined) =>
        this.#authRequest<Workout>(`/workouts/${id}?geolocation=true`);

    getUser = () => this.#authRequest<User>('/users/current');

    getAllLabels = () => this.#authRequest<LabelType[]>('/labels');

    deleteWorkout = (id: string | undefined) =>
        this.#authRequest<string>(`/workouts/${id}`, DELETE);

    previewWorkout = (data: UploadWorkout) =>
        this.#authRequest<WorkoutPreview>('/workouts/preview', POST, data);

    addWorkout = (data: UploadWorkout) =>
        this.#authRequest<Workout>('/workouts', POST, data);

    updateWorkout = (id: string | undefined, data: UpdateWorkout) =>
        this.#authRequest<Workout>(`/workouts/${id}`, PUT, data);
}
