import {
    AdminGetUserCommand,
    AdminInitiateAuthCommand,
    AdminListGroupsForUserCommand,
    AuthFlowType,
    CognitoIdentityProviderClient,
    ConfirmForgotPasswordCommand,
    ForgotPasswordCommand,
    ResendConfirmationCodeCommand,
    SignUpCommand,
} from '@aws-sdk/client-cognito-identity-provider';
import { createHmac } from 'crypto';

export class CognitoApi {
    #provider: CognitoIdentityProviderClient;
    #clientId: string;
    #userPoolId: string;
    #clientSecret: string;

    constructor() {
        const clientId = process.env.COGNITO_CLIENT_ID;
        const userPoolId = process.env.COGNITO_USER_POOL_ID;
        const clientSecret = process.env.COGNITO_CLIENT_SECRET;

        if (!clientId)
            throw new Error('AWS Cognito client id was not provided');
        if (!userPoolId)
            throw new Error('AWS Cognito user pool id was not provided');
        if (!clientSecret)
            throw new Error('AWS Cognito client secret was not provided');

        this.#provider = new CognitoIdentityProviderClient();
        this.#clientId = clientId;
        this.#userPoolId = userPoolId;
        this.#clientSecret = clientSecret;
    }

    #getHashSecret = (username: string) =>
        createHmac('SHA256', this.#clientSecret)
            .update(username + this.#clientId)
            .digest('base64');

    signup = (email: string, password: string, name: string, isDemo: boolean) =>
        this.#provider.send(
            new SignUpCommand({
                ClientId: this.#clientId,
                Password: password,
                Username: email,
                SecretHash: this.#getHashSecret(email),
                UserAttributes: [
                    { Name: 'name', Value: name },
                    { Name: 'custom:is_demo', Value: isDemo.toString() },
                ],
            })
        );

    resendConfirmationCode = (email: string) =>
        this.#provider.send(
            new ResendConfirmationCodeCommand({
                ClientId: this.#clientId,
                SecretHash: this.#getHashSecret(email),
                Username: email,
            })
        );

    login = (email: string, password: string) =>
        this.#provider.send(
            new AdminInitiateAuthCommand({
                ClientId: this.#clientId,
                UserPoolId: this.#userPoolId,
                AuthFlow: AuthFlowType.ADMIN_USER_PASSWORD_AUTH,
                AuthParameters: {
                    USERNAME: email,
                    PASSWORD: password,
                    SECRET_HASH: this.#getHashSecret(email),
                },
            })
        );

    refeshToken = (refreshToken: string, email: string) =>
        this.#provider.send(
            new AdminInitiateAuthCommand({
                ClientId: this.#clientId,
                UserPoolId: this.#userPoolId,
                AuthFlow: AuthFlowType.REFRESH_TOKEN_AUTH,
                AuthParameters: {
                    REFRESH_TOKEN: refreshToken,
                    SECRET_HASH: this.#getHashSecret(email),
                },
            })
        );

    getUser = (email: string) =>
        this.#provider.send(
            new AdminGetUserCommand({
                UserPoolId: this.#userPoolId,
                Username: email,
            })
        );

    getUserGroups = (email: string) =>
        this.#provider.send(
            new AdminListGroupsForUserCommand({
                UserPoolId: this.#userPoolId,
                Username: email,
            })
        );

    requestPasswordReset = (email: string) =>
        this.#provider.send(
            new ForgotPasswordCommand({
                ClientId: this.#clientId,
                SecretHash: this.#getHashSecret(email),
                Username: email,
            })
        );

    resetPassword = (
        email: string,
        password: string,
        confirmationCode: string
    ) =>
        this.#provider.send(
            new ConfirmForgotPasswordCommand({
                ClientId: this.#clientId,
                SecretHash: this.#getHashSecret(email),
                Username: email,
                Password: password,
                ConfirmationCode: confirmationCode,
            })
        );
}
