import type { ActionFunctionArgs } from '@remix-run/node';
import { json, redirect } from '@remix-run/node';

import { Api } from './api/api.server';
import { handleError } from './helpers.server';

export const uploadWorkoutData = async (
    { request, params }: ActionFunctionArgs,
    isEdit: boolean
) => {
    try {
        const formData = await request.formData();
        const workoutData = formData.get('workoutData');
        const isFinal = !!formData.get('final');
        const body = JSON.parse(workoutData as string);

        const api = new Api();
        await api.setToken(request);

        const response = await api.previewWorkout(body);

        if (isFinal) {
            if (isEdit) {
                await api.updateWorkout(params?.id as string, body);
                return redirect('/dashboard/running');
            }

            const finalResponse = await api.addWorkout(body);

            if (finalResponse) {
                return json({ success: true });
            }
            return json({ success: false });
        }
        return json(response);
    } catch (e) {
        return handleError(e, request);
    }
};
