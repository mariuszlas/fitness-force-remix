import { AxiosError } from 'axios';

import { logout } from '~/server/session/authSession.server';

type ErrorWithMessage = { message: string };

type ApiErrorData = {
    timestamp: string;
    status: number;
    error: string;
    message: string;
    path: string;
};

type AxiosApiErrorWithMessage = AxiosError<ApiErrorData>;

export const isAxiosApiError = (e: unknown): e is AxiosApiErrorWithMessage =>
    e instanceof AxiosError &&
    typeof e.response?.data === 'object' &&
    e.response?.data !== null;

export const isErrorWithMessage = (e: unknown): e is ErrorWithMessage =>
    typeof e === 'object' &&
    e !== null &&
    'message' in e &&
    typeof e.message === 'string';

export const getGenericErrorMessage = (e: unknown, fallback: string) =>
    isErrorWithMessage(e) ? e.message : fallback;

export const handleError = (e: unknown, request: Request) => {
    if (
        isAxiosApiError(e) &&
        (e?.response?.status === 401 || e?.response?.status === 403)
    ) {
        console.error(e.message);
        return logout(request);
    }

    throw new Error();
};
