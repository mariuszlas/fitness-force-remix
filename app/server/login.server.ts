import {
    NotAuthorizedException,
    UserNotConfirmedException,
    UserNotFoundException,
} from '@aws-sdk/client-cognito-identity-provider';
import { redirect } from '@remix-run/node';

import { Api } from './api/api.server';
import { CognitoApi } from './api/cognitoAuth.server';
import { getGenericErrorMessage } from './helpers.server';
import {
    handleOtherError,
    handleValidationError,
    validateEmail,
    ValidationError,
} from './validation.server';

import { commitSession, getSession } from '~/server/session/authSession.server';

export const loginUser = async (request: Request) => {
    const session = await getSession(request);

    const formData = await request.formData();
    const email = formData.get('email')?.toString() ?? '';
    const password = formData.get('password')?.toString() ?? '';

    try {
        validateEmail(email);

        const response = await new CognitoApi().login(email, password);

        const accessToken = response.AuthenticationResult?.AccessToken;
        const refreshToken = response.AuthenticationResult?.RefreshToken;
        const idToken = response.AuthenticationResult?.IdToken;

        if (
            response.$metadata.httpStatusCode !== 200 ||
            !accessToken ||
            !refreshToken ||
            !idToken
        ) {
            throw new Error();
        }

        const api = new Api();
        api.setTokens(idToken, refreshToken);
        await api.reportUserLogin();

        session.set('idToken', idToken);
        session.set('refreshToken', refreshToken);

        return redirect('/dashboard/running', {
            headers: {
                'Set-Cookie': await commitSession(session),
            },
        });
    } catch (e) {
        if (e instanceof ValidationError) {
            return handleValidationError(e);
        }

        if (e instanceof UserNotConfirmedException) {
            return redirect(`/verify?user=${email}`);
        }

        if (
            e instanceof UserNotFoundException ||
            e instanceof NotAuthorizedException
        ) {
            return handleOtherError('Incorrect login credentials');
        }

        const msg = getGenericErrorMessage(e, 'Login failed');
        console.error('Error: ' + msg);
        return handleOtherError('Login failed', false);
    }
};
