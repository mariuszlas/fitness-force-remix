import type { Session } from '@remix-run/node';
import { createCookieSessionStorage, redirect } from '@remix-run/node';

type SessionData = {
    refreshToken: string;
    idToken: string;
    email: string;
    username: string;
};

type SessionFlashData = {
    error: string;
};

export type AuthSession = Session<SessionData, SessionFlashData>;

const REFRESH_TOKEN = 'refreshToken';
const ID_TOKEN = 'idToken';

const sessionStorage = createCookieSessionStorage<
    SessionData,
    SessionFlashData
>({
    cookie: {
        name: '__session',
        httpOnly: true,
        maxAge: 60 * 60 * 24 * 7,
        sameSite: 'lax',
        secrets: [process.env.SESSION_SECRET as string],
        secure: process.env.NODE_ENV === 'production',
    },
});

export const { commitSession } = sessionStorage;

export const getSession = (request: Request) => {
    const cookie = request.headers.get('Cookie');
    return sessionStorage.getSession(cookie);
};

export const getSessionTokens = (session: AuthSession) => ({
    [ID_TOKEN]: session.get(ID_TOKEN),
    [REFRESH_TOKEN]: session.get(REFRESH_TOKEN),
});

export const decodeJwt = (jwt: string | undefined) =>
    jwt ? JSON.parse(Buffer.from(jwt.split('.')[1], 'base64').toString()) : {};

export const getSessionUser = (session: AuthSession) => {
    const decoded = decodeJwt(session.get(ID_TOKEN));
    if (isCognitoUser(decoded)) {
        return {
            username: decoded['cognito:username'],
            email: decoded.email,
            name: decoded.name,
            emailVerified: decoded.email_verified,
            isDemo: decoded['custom:is_demo'] === 'true' ? true : false,
        };
    }
    throw new Error();
};

export const getSessionEmail = async (request: Request) =>
    getSessionUser(await getSession(request)).email;

export const logout = async (request: Request) => {
    const session = await getSession(request);

    return redirect('/login', {
        headers: {
            'Set-Cookie': await sessionStorage.destroySession(session),
        },
    });
};

export const requireUserSession = async (request: Request) => {
    const session = await getSession(request);
    if (!session.has(ID_TOKEN)) {
        throw await logout(request);
    }

    return session;
};

type CognitoUser = {
    sub: string;
    aud: string;
    email_verified: boolean;
    token_use: string;
    auth_time: number;
    iss: string;
    'cognito:username': string;
    origin_jti: string;
    event_id: string;
    jti: string;
    exp: number;
    iat: number;
    email: string;
    name: string;
    'custom:is_demo': string;
};

const isCognitoUser = (o: unknown): o is CognitoUser =>
    typeof o === 'object' &&
    o !== null &&
    'sub' in o &&
    'aud' in o &&
    'email_verified' in o &&
    'token_use' in o &&
    'auth_time' in o &&
    'iss' in o &&
    'cognito:username' in o &&
    'exp' in o &&
    'iat' in o &&
    'email' in o &&
    'origin_jti' in o &&
    'event_id' in o &&
    'jti' in o &&
    'name' in o &&
    'custom:is_demo' in o;
