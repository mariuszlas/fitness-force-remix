import { createCookieSessionStorage } from '@remix-run/node';

import { Theme } from '../../providers';
import { isTheme } from '../../providers/themeProvider';

type SessionData = {
    theme: Theme;
};

type SessionFlashData = {
    error: string;
};

const THEME = 'theme';

const themeStorage = createCookieSessionStorage<SessionData, SessionFlashData>({
    cookie: {
        name: 'remix_theme',
        httpOnly: true,
        sameSite: 'lax',
        secrets: [process.env.SESSION_SECRET as string],
        secure: process.env.NODE_ENV === 'production',
    },
});

export const getThemeSession = async (request: Request) => {
    const session = await themeStorage.getSession(
        request.headers.get('Cookie')
    );
    return {
        getTheme: () => {
            const themeValue = session.get(THEME);
            return isTheme(themeValue) ? themeValue : Theme.LIGHT;
        },
        setTheme: (theme: Theme) => session.set(THEME, theme),
        commit: () => themeStorage.commitSession(session),
    };
};

export const getThemeFromRequest = async (request: Request) => {
    const requestText = await request.text();
    const form = new URLSearchParams(requestText);
    return form.get(THEME);
};
