import { UsernameExistsException } from '@aws-sdk/client-cognito-identity-provider';
import { redirect } from '@remix-run/node';

import { Api } from './api/api.server';
import { CognitoApi } from './api/cognitoAuth.server';
import { getGenericErrorMessage, isAxiosApiError } from './helpers.server';
import {
    handleOtherError,
    handleValidationError,
    validateEmail,
    validateName,
    validatePassword,
    ValidationError,
} from './validation.server';

import { _t } from '~/constants';

export const signupUser = async (
    request: Request,
    acceptNewUsers?: boolean,
    isDemo?: boolean
) => {
    if (!acceptNewUsers) {
        return handleOtherError(_t.errorNewUsers);
    }

    try {
        const formData = await request.formData();

        const email = formData.get('email')?.toString() ?? '';
        const name = formData.get('name')?.toString() ?? '';
        const password = formData.get('password')?.toString() ?? '';
        const passwordRepeat = formData.get('passwordRepeat')?.toString() ?? '';

        validateEmail(email);
        validateName(name);
        validatePassword(password, passwordRepeat);

        const response = await new CognitoApi().signup(
            email,
            password,
            name,
            !!isDemo
        );

        const username = response.UserSub;

        if (response.$metadata.httpStatusCode !== 200 || !username) {
            throw new Error('Unexpected error occured');
        }

        await new Api().createUser({ email, username });

        return redirect(`/verify?user=${email}`);
    } catch (e) {
        if (e instanceof ValidationError) {
            return handleValidationError(e);
        }

        if (e instanceof UsernameExistsException) {
            return handleOtherError(e.message);
        }

        if (isAxiosApiError(e)) {
            return handleOtherError(e?.response?.data?.message);
        }

        const msg = getGenericErrorMessage(e, 'User signup failed');
        console.error('Error: ' + msg);

        return handleOtherError('User signup failed', false);
    }
};
