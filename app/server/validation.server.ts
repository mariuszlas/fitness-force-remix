import { json } from '@remix-run/node';

import { _t } from '~/constants';

export class ValidationError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'ValidationError';
    }
}

export class EmailError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = 'EmailError';
    }
}

export class PasswordError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = 'PasswordError';
    }
}

export class NameError extends ValidationError {
    constructor(message: string) {
        super(message);
        this.name = 'NameError';
    }
}

export const isEmail = (text: string) =>
    // eslint-disable-next-line no-useless-escape
    /^[\w-\.]+@([\w-]+\.)+[\w-]{2,5}$/.test(text);

export const validateEmail = (email: string) => {
    if (!isEmail(email)) {
        throw new EmailError('Email must be a valid email address');
    }
};

export const validatePassword = (passwd1: string, passwd2: string) => {
    if (passwd1 !== passwd2) {
        throw new PasswordError(_t.errorPassword1);
    }
    if (passwd1.length < 8) {
        throw new PasswordError(_t.errorPassword2);
    }
    if (passwd1 === 'Password') {
        throw new PasswordError(_t.errorPassword3);
    }
    if (passwd1 === 'password') {
        throw new PasswordError(_t.errorPassword4);
    }
    if (!/[A-Z]/g.test(passwd1)) {
        throw new PasswordError(_t.errorPassword5);
    }
    if (!/[0-9]/g.test(passwd1)) {
        throw new PasswordError(_t.errorPassword6);
    }
};

export const validateName = (name: string) => {
    if (name.length < 1) {
        throw new NameError('Username cannot be empty');
    }
};

export interface errs {
    email: string | null;
    name: string | null;
    password: string | null;
    other: string | null;
}

const errs: errs = {
    email: null,
    name: null,
    password: null,
    other: null,
};

const formatError = (errors: errs) => json({ ok: false, errors });

export const handleValidationError = (e: ValidationError) => {
    const message = e.message;

    if (e instanceof EmailError) {
        console.error('EmailError: ' + message);
        return formatError({ ...errs, email: message });
    } else if (e instanceof PasswordError) {
        console.error('PasswordError: ' + message);
        return formatError({ ...errs, password: message });
    } else {
        console.error('NameError: ' + message);
        return formatError({ ...errs, name: message });
    }
};

export const handleOtherError = (
    message: string | undefined,
    logError = true
) => {
    if (logError) {
        console.error('Error: ' + message);
    }
    return formatError({ ...errs, other: message ?? 'Unexpected error' });
};
