import { cn, updateLocalStorage } from './helpers';

describe('utils/helpers', () => {
    describe('updateLocalStorage', () => {
        const testKey = 'color-mode';

        beforeEach(() => {
            localStorage.clear();
        });

        it('should set the value in localStorage when storedValue is provided', () => {
            updateLocalStorage(testKey, 'dark');
            const storedValue = localStorage.getItem(testKey);
            expect(storedValue).toBe('dark');
        });

        it('should remove the value from localStorage when storedValue is null', () => {
            localStorage.setItem(testKey, 'light');
            updateLocalStorage(testKey, undefined);
            const storedValue = localStorage.getItem(testKey);
            expect(storedValue).toBeNull();
        });
    });

    describe('cn', () => {
        it('merges class names correctly', () => {
            const expectedClassName =
                'bg-blue-500 hover:bg-blue-700 font-bold text-white';
            const actualClassName = cn(
                'bg-blue-500',
                'hover:bg-blue-700',
                'font-bold text-white'
            );
            expect(actualClassName).toBe(expectedClassName);
        });
    });
});
