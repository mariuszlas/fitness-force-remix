import type { ClassValue } from 'clsx';
import { clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

export const cn = (...inputs: ClassValue[]) => twMerge(clsx(inputs));

export const updateLocalStorage = (
    key: string,
    storedValue: string | undefined
) => {
    if (typeof window !== 'object') return;

    try {
        if (storedValue !== undefined) {
            localStorage.setItem(key, storedValue);
        } else {
            localStorage.removeItem(key);
        }
    } catch (_) {
        return;
    }
};
