import { Fragment, useState } from 'react';
import { usePopper } from 'react-popper';
import { Popover } from '@headlessui/react';
import { Form } from '@remix-run/react';

import {
    _t,
    Heading,
    IconButton,
    MenuButton,
    MenuLink,
    MenuTransition,
    Text,
    UserIcon,
} from '~/components';
import { useUser } from '~/providers';

export const AccountMenu = () => {
    const [menuBtnElement, setMenuBtnElement] =
        useState<HTMLButtonElement | null>(null);
    const [menuElement, setMenuElement] = useState<HTMLDivElement | null>(null);

    const { styles, attributes } = usePopper(menuBtnElement, menuElement, {
        placement: 'bottom-end',
        modifiers: [{ name: 'offset', options: { offset: [0, 8] } }],
    });

    const { email, name } = useUser();

    return (
        <Popover as="div">
            <Popover.Button as={Fragment}>
                <IconButton aria-label="Account menu" ref={setMenuBtnElement}>
                    <UserIcon />
                </IconButton>
            </Popover.Button>

            <MenuTransition>
                <Popover.Panel
                    ref={setMenuElement}
                    style={styles.popper}
                    className="z-30 w-56 rounded-lg border border-base-content border-opacity-20 bg-base-100 p-2 shadow-2xl focus:outline-none"
                    {...attributes.popper}
                    role="menu"
                >
                    {name && email && (
                        <>
                            <div className="flex flex-col gap-2">
                                <Heading
                                    as="h2"
                                    className="overflow-hidden text-ellipsis px-4"
                                    title="user name"
                                >
                                    {name}
                                </Heading>
                                <Text
                                    className="overflow-hidden text-ellipsis px-4 text-sm"
                                    title="user email"
                                >
                                    {email}
                                </Text>
                            </div>

                            <hr className="my-2 border-t border-t-base-content border-opacity-20 " />
                        </>
                    )}

                    <MenuLink
                        to="best-results"
                        relative="path"
                        role="menuitem"
                        popover
                    >
                        {_t.bestResults}
                    </MenuLink>

                    <MenuLink to="user" relative="path" role="menuitem" popover>
                        {_t.accountSettings}
                    </MenuLink>

                    <hr className="my-2 border-t border-t-base-content border-opacity-20 " />

                    <Form method="POST">
                        <MenuButton
                            type="submit"
                            formAction="/logout"
                            role="menuitem"
                            hoverRed
                        >
                            {_t.signOut}
                        </MenuButton>
                    </Form>
                </Popover.Panel>
            </MenuTransition>
        </Popover>
    );
};
