import type { FC } from 'react';

import { _t } from '..';

import { LineItem } from './accountSettingsLineItem';

import type { UserData } from '~/interfaces';

interface Props {
    user: UserData | undefined;
    isLoading?: boolean;
}

export const AccountSettings: FC<Props> = ({ user, isLoading }) => (
    <div className="mt-6 flex flex-col gap-4">
        <LineItem
            header={_t.labelName}
            value={user?.name}
            isLoading={isLoading}
        />
        <LineItem
            header={_t.labelEmail}
            value={user?.email}
            isLoading={isLoading}
        />
        <LineItem
            header={_t.labelPass}
            value={_t.plcdPassword}
            isLoading={isLoading}
        />

        <div className="flex justify-between">
            <LineItem
                header={_t.dateCreated}
                value={new Date(user?.createdAt ?? '').toLocaleDateString()}
                isLoading={isLoading}
                noAction
            />
            <LineItem
                header={_t.lastLogin}
                value={new Date(user?.lastLogin ?? '').toLocaleString()}
                isLoading={isLoading}
                noAction
            />
        </div>

        <LineItem
            header={_t.accountDeletion}
            isLoading={isLoading}
            deleteAccount
        />
    </div>
);
