import type { FC } from 'react';
import { useState } from 'react';
import { Form, useNavigation } from '@remix-run/react';

import {
    Alert,
    Button,
    EyeIcon,
    EyeSlashIcon,
    IconButton,
    Input,
    Text,
} from '~/components';
import { _t } from '~/constants';
import type { errs } from '~/server/validation.server';

type ActionData = {
    ok: boolean;
    errors: errs;
};

interface Props {
    email: string | undefined;
    actionData: ActionData | undefined;
}

export const ResetPasswordForm: FC<Props> = ({ email, actionData }) => {
    const navigation = useNavigation();
    const [showPassword, setShowPassword] = useState(false);
    const [showPasswordRepeat, setShowPasswordRepeat] = useState(false);

    const otherError = actionData?.errors?.other;
    const nameError = actionData?.errors?.name;
    const passwordError = actionData?.errors?.password;

    return (
        <Form method="POST" className="form-control w-full gap-6">
            <div className="form-control gap-2">
                {otherError && (
                    <Alert status="error" classes="mb-0">
                        {otherError}
                    </Alert>
                )}

                <Text as="p">{_t.passwordResetBody2}</Text>

                <div>
                    <Input
                        required
                        autoFocus
                        id="passwordResetCode"
                        name="passwordResetCode"
                        type="text"
                        label={_t.labelPasswordResetCode}
                        error={nameError ?? undefined}
                        aria-invalid={Boolean(nameError)}
                        aria-errormessage={nameError ?? undefined}
                    />
                </div>

                <Input
                    className="hidden"
                    value={email}
                    readOnly
                    required
                    id="email"
                    name="email"
                    type="email"
                />

                <div>
                    <Input
                        required
                        id="password"
                        name="password"
                        type={showPassword ? 'text' : 'password'}
                        className={'pr-10'}
                        label={_t.labelPass}
                        placeholder={_t.plcdPassword}
                        error={passwordError ?? undefined}
                        aria-invalid={Boolean(passwordError)}
                        aria-errormessage={passwordError ?? undefined}
                    >
                        <IconButton
                            onClick={() => setShowPassword(prev => !prev)}
                            className="absolute right-0"
                            aria-label="toggle password visibility"
                        >
                            {showPassword ? <EyeSlashIcon /> : <EyeIcon />}
                        </IconButton>
                    </Input>
                </div>

                <div>
                    <Input
                        required
                        id="passwordRepeat"
                        name="passwordRepeat"
                        type={showPasswordRepeat ? 'text' : 'password'}
                        className={'pr-10'}
                        label={_t.labelPassRep}
                        placeholder={_t.plcdPassword}
                        error={passwordError ?? undefined}
                        aria-invalid={Boolean(passwordError)}
                        aria-errormessage={passwordError ?? undefined}
                    >
                        <IconButton
                            onClick={() => setShowPasswordRepeat(prev => !prev)}
                            className="absolute right-0"
                            aria-label="toggle password repeat visibility"
                        >
                            {showPasswordRepeat ? (
                                <EyeSlashIcon />
                            ) : (
                                <EyeIcon />
                            )}
                        </IconButton>
                    </Input>
                </div>
            </div>

            <Button
                type="submit"
                isLoading={navigation.state === 'submitting'}
                className="btn-primary btn-block"
            >
                {_t.passwordResetBtn}
            </Button>
        </Form>
    );
};
