import type { FC } from 'react';

import { LineItem } from './bestResultsLineItem';

import { _t, Skeleton } from '~/components';
import type {
    BestResults as BestResultsType,
    WorkoutTypes,
} from '~/interfaces';

const running = [
    { key: 'one_k', value: '1K' },
    { key: 'five_k', value: '5K' },
    { key: 'ten_k', value: '10K' },
    { key: 'half_marathon', value: 'Half Marathon' },
    { key: 'marathon', value: 'Marathon' },
];

const walking = [
    { key: 'ten_k', value: '10K' },
    { key: 'thirty_k', value: '30K' },
    { key: 'fifty_k', value: '50K' },
];

const cycling = [
    { key: 'thirty_k', value: '30K' },
    { key: 'fifty_k', value: '50K' },
    { key: 'hundred_k', value: '100K' },
    { key: 'one_hundred_fifty_k', value: '150K' },
    { key: 'two_hundred_k', value: '200K' },
];

const allKeys = { running, walking, cycling };

interface Props {
    data: BestResultsType | undefined;
    workoutType: WorkoutTypes;
    isLoading?: boolean;
}

export const BestResults: FC<Props> = ({ data, workoutType, isLoading }) => {
    const keys = workoutType ? allKeys[workoutType] : null;

    return (
        <ul className="mt-6 flex w-full flex-col gap-4">
            {isLoading
                ? keys?.map(({ key, value }) => (
                      <Skeleton
                          key={key + value}
                          h={14}
                          className="block h-14 w-full"
                      />
                  ))
                : keys?.map(({ key, value }, idx) => (
                      <LineItem key={idx} data={data?.[key]} header={value} />
                  ))}
        </ul>
    );
};
