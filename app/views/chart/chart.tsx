import type { FC, MouseEvent } from 'react';
import { useEffect, useRef, useState } from 'react';
import Chart from 'chart.js/auto';

import {
    destroyChart,
    getChartConfig,
    getSecondaryStat,
    updateChart,
    updateChartTheme,
} from './helpers';

import { useTheme, useUI } from '~/providers';

export type ChartType = 'bar';
export type BarChartData = { x: string; y: number }[] | null;
export type BarChartT = Chart<ChartType, BarChartData, string[]>;

interface ChartProps {
    chartData: BarChartData;
}

export interface ChartTheme {
    barColor: string;
    textColor: string;
    gridDashColor: string;
}

export const BarChart: FC<ChartProps> = ({ chartData }) => {
    const { setSecondaryStat } = useUI();
    const ref = useRef<HTMLCanvasElement>(null);
    const [theme] = useTheme();
    const [chart, setChart] = useState<BarChartT>();

    const barColorLight = '#14b8a6';
    const barColorDark = '#14b8a6';
    const textColorLight = '#1f2937';
    const textColorDark = '#9ca3af';
    const gridDashColor = '#6b7280';

    // const barColorLight = colors.teal[500];
    // const barColorDark = colors.teal[500];
    // const textColorLight = colors.gray[800];
    // const textColorDark = colors.gray[400];
    // const gridDashColor = colors.gray[500];

    const handleBarClick = (event: MouseEvent<HTMLCanvasElement>) => {
        const secondaryStat = getSecondaryStat(event, chart, chartData);
        if (secondaryStat) setSecondaryStat(secondaryStat);
    };

    const getChartThemeValues = (theme: string) => ({
        barColor: theme === 'light' ? barColorLight : barColorDark,
        textColor: theme === 'light' ? textColorLight : textColorDark,
        gridDashColor,
    });

    useEffect(() => {
        updateChart(chart, chartData);
    }, [chartData]);

    useEffect(() => {
        if (!ref.current) return;

        const chartConfig = getChartConfig(
            chartData,
            getChartThemeValues(theme as string)
        );
        const newChart = new Chart(ref.current, chartConfig);
        setChart(newChart);

        return () => destroyChart(newChart);
    }, []);

    useEffect(() => {
        updateChartTheme(chart, getChartThemeValues(theme as string));
    }, [theme]);

    const fallbackContent = <p>Workouts chart</p>;

    return (
        <canvas
            ref={ref}
            role="img"
            aria-label="chart"
            onClick={e => handleBarClick(e)}
        >
            {fallbackContent}
        </canvas>
    );
};
