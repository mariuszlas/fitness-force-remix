export const MAX_BAR_THICKNESS = 25;
export const TICK_FONT =
    "Inter -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif";
export const TICK_FONT_SIZE = 16;
export const TEXT_LIGHT = '#6c757d';
export const TEXT_DARK = '#adb5bd';
export const Y_BORDER_DASH = [4, 6];
export const BAR_TYPE = 'bar';
export const MAX_TICK_LIMIT = 10;
