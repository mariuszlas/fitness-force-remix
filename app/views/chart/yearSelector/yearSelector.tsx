import { type ChangeEvent, useEffect, useMemo } from 'react';
import { useMatches } from '@remix-run/react';

import {
    _t,
    ArrowLeft,
    ArrowRight,
    IconButton,
    Select,
} from '../../../components';
import {
    findBestMonths,
    getAvailableYears,
    getNextYearIdx,
    getPreviousYearIdx,
    getSecStats,
} from '../helpers';

import type { BestMonths, WorkoutsDashboard, WorkoutTypes } from '~/interfaces';
import { useUI } from '~/providers';

interface Props {
    availableYears?: number[];
    bestMonths?: BestMonths | undefined;
    dashboard: WorkoutsDashboard;
}

const NEXT = 'NEXT';
const PREV = 'PREVIOUS';

export const YearSelector: React.FC<Props> = ({ dashboard }) => {
    const matches = useMatches();
    const { year, setYear, setSecondaryStat } = useUI();

    const workoutType = matches.at(2)?.params?.workoutType as WorkoutTypes;

    const availableYears = useMemo(
        () => getAvailableYears(dashboard),
        [dashboard, workoutType]
    );

    const bestMonths = useMemo(
        () => findBestMonths(dashboard),
        [dashboard, workoutType]
    );

    useEffect(() => {
        const secStat = getSecStats(year, bestMonths);
        setSecondaryStat(secStat);
    }, [workoutType]);

    if (!availableYears?.length) return null;

    const getOptions = (availableYears: number[]) => {
        const options = availableYears.map((year, idx) => (
            <option key={idx + 1} value={year}>
                {year}
            </option>
        ));
        options.unshift(
            <option key={0} value={0}>
                {_t.total}
            </option>
        );
        return options;
    };

    const onSelectYear = (e: ChangeEvent<HTMLSelectElement>) => {
        const newYear = Number(e.target.value);
        const secStat = getSecStats(newYear, bestMonths);

        setYear(newYear);
        setSecondaryStat(secStat);
    };

    const onChangeYear = (direction: string) => {
        const years = [...availableYears];
        years.unshift(0);
        const currIdx = years.indexOf(year);

        const newIdx =
            direction === NEXT
                ? getNextYearIdx(currIdx)
                : getPreviousYearIdx(currIdx, years.length);

        const newYear = years[newIdx];
        const secStat = getSecStats(newYear, bestMonths);

        setYear(newYear);
        setSecondaryStat(secStat);
    };

    return (
        <div
            className="flex items-center gap-3"
            data-testid="data-year-selector"
        >
            <IconButton
                aria-label="Select next year"
                onClick={() => onChangeYear(NEXT)}
            >
                <ArrowLeft />
            </IconButton>

            <Select
                className="w-full max-w-xs"
                value={year}
                data-testid="data-year-selector-dropdown"
                aria-label="select year"
                onChange={onSelectYear}
            >
                {getOptions(availableYears)}
            </Select>

            <IconButton
                aria-label="Select previous year"
                onClick={() => onChangeYear(PREV)}
            >
                <ArrowRight />
            </IconButton>
        </div>
    );
};
