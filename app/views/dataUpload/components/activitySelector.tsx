import type { FC } from 'react';

import { capitalize } from '../../helpers';

import { _t, FormLabel, Select } from '~/components';
import { WorkoutTypes } from '~/interfaces';

export interface ActivitySelectorProps {
    type: string;
    setType: (type: WorkoutTypes) => void;
}

export const ActivitySelector: FC<ActivitySelectorProps> = ({
    type,
    setType,
}) => (
    <div className="w-fit">
        <FormLabel text={_t.selectActivity} htmlFor="activity" />
        <Select
            id="activity"
            className="w-full"
            value={type}
            onChange={e => setType(e.target.value as WorkoutTypes)}
        >
            {Object.values(WorkoutTypes).map(
                (workoutType: WorkoutTypes, idx: number) => (
                    <option key={idx} value={workoutType}>
                        {capitalize(workoutType)}
                    </option>
                )
            )}
        </Select>
    </div>
);
