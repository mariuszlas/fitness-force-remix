import type { FC } from 'react';

import { _t, FormLabel, Input } from '~/components';
import { useIsBreakpoint } from '~/hooks';

export interface DistanceInputProps {
    distance: string;
    setDistance: (distance: string) => void;
}

export const DistanceInput: FC<DistanceInputProps> = ({
    distance,
    setDistance,
}) => {
    const isMobile = useIsBreakpoint('sm');

    return (
        <div className="w-fit">
            <FormLabel
                text={isMobile ? _t.distanceKmShort : _t.distanceKm}
                htmlFor="distance"
            />
            <Input
                id="distance"
                className="w-20 sm:w-24"
                value={distance}
                type="number"
                min={0}
                onChange={e => setDistance(e.target.value)}
            />
        </div>
    );
};
