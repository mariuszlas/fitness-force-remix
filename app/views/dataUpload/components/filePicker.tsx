import type { FC } from 'react';
import { useRef, useState } from 'react';

import { parseXML, readFileAsync } from '../xmlParser';

import { _t, Alert, Badge, Button, Text } from '~/components';
import { WorkoutTypes } from '~/interfaces';

export interface FilePickerProps {
    setDistance: (distance: string) => void;
    setTimestamp: (distance: string) => void;
    setUtcOffset: (distance: number) => void;
    setDuration: (distance: number) => void;
    setType: (distance: string) => void;
    setCoordinates: (distance: [number, number][]) => void;
    file: File | null;
    setFile: (file: File | null) => void;
}

export const FilePicker: FC<FilePickerProps> = ({
    setDistance,
    setTimestamp,
    setUtcOffset,
    setDuration,
    setType,
    setCoordinates,
    file,
    setFile,
}) => {
    const [error, setError] = useState<string | null>(null);
    const ref = useRef<HTMLInputElement>(null);

    const readFile = async (file: File) => {
        try {
            const xmlString = await readFileAsync(file);

            if (typeof xmlString !== 'string') {
                throw Error(_t.errorParsingFile);
            }
            const data = parseXML(xmlString, file.name);

            setDistance(data.distance.toString());
            setTimestamp(data.timestamp.split('Z')[0]);
            setUtcOffset(new Date().getTimezoneOffset() / -60);
            setDuration(data.duration);
            setType(data.type);
            setCoordinates(data.coordinates);
            setFile(file);
        } catch (e) {
            setError(_t.errorParsingFile);
        }
    };

    const onInputChange = async () => {
        if (ref.current && ref.current.files) {
            const file = ref.current.files[0];
            await readFile(file);
        }
    };

    const onButtonClick = () => {
        if (ref && ref.current) {
            setError(null);
            ref.current.click();
        }
    };

    const removeFile = () => {
        setFile(null);
        setDistance('');
        setTimestamp('');
        setUtcOffset(0);
        setDuration(0);
        setType(WorkoutTypes.RUNNING);
        setCoordinates([]);
    };

    return (
        <div>
            {error && (
                <Alert status="error" classes="my-4">
                    {error}
                </Alert>
            )}

            <div className="flex items-center justify-between gap-4">
                {file ? (
                    <Badge value={file.name} onClose={removeFile} />
                ) : (
                    <Text className="italic">{_t.noFile}</Text>
                )}

                <input
                    type="file"
                    ref={ref}
                    className="hidden"
                    onChange={onInputChange}
                    accept=".tcx"
                />

                <Button
                    className="btn-primary btn-outline"
                    onClick={onButtonClick}
                >
                    {_t.btnFileInput}
                </Button>
            </div>
        </div>
    );
};
