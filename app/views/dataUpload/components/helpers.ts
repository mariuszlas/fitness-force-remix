import { getHMS } from '../../helpers';

import { _t, labelColors } from '~/constants';
import type { LabelType } from '~/interfaces';

export const zeroPad = (n: number | string) =>
    Number(n) < 10 ? `0${n}` : n.toString();

export const zeroPadDuration = (duration: number) =>
    `${zeroPad(getHMS(duration).h)}:${zeroPad(getHMS(duration).m)}:${zeroPad(
        getHMS(duration).s
    )}`;

export const formatNewLabelValue = (newLabelValue: string) =>
    newLabelValue.replace(/^\s+|\s+$/g, '');

export const validateNewLabel = (
    allLabels: LabelType[],
    newLabels: LabelType[],
    newLabelValue: string
): void => {
    const formatedNewLabelValue = formatNewLabelValue(newLabelValue);

    if (formatedNewLabelValue.length === 0 || formatedNewLabelValue.length > 25)
        throw Error(_t.errorLabelInputLength);

    if (/;/g.test(formatedNewLabelValue))
        throw Error(_t.errorLabelInputBadChar);

    allLabels.concat(newLabels).forEach(label => {
        if (label.value === formatedNewLabelValue)
            throw Error(_t.errorLabelDuplicate);
    });
};

export const getNewLabel = (
    allLabels: LabelType[],
    newLabels: LabelType[],
    newLabelValue: string
): LabelType => {
    const formatedNewLabelValue = formatNewLabelValue(newLabelValue);
    const takenColors = allLabels.concat(newLabels).map(label => label.color);
    const avilableColors = labelColors.filter(
        label => !takenColors.includes(label)
    );
    const colors = avilableColors.length === 0 ? takenColors : avilableColors;
    const randomNum = Math.floor(Math.random() * colors.length);

    return { value: formatedNewLabelValue, color: colors[randomNum] };
};

export const getHours = (seconds: number) => getHMS(seconds).h;

export const getMinues = (seconds: number) => getHMS(seconds).m;

export const getSeconds = (seconds: number) => getHMS(seconds).s;
