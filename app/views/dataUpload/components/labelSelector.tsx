import type { FC, MouseEvent } from 'react';
import { Fragment, useState } from 'react';
import { usePopper } from 'react-popper';
import { Popover } from '@headlessui/react';

import { getNewLabel, validateNewLabel } from './helpers';

import {
    _t,
    Button,
    IconButton,
    Input,
    Label,
    MenuButton,
    MenuTransition,
    PlusIcon,
    SkeletonList,
    Text,
} from '~/components';
import type { LabelType } from '~/interfaces';
import { getGenericErrorMessage } from '~/server/helpers.server';

interface LabelSelectorProps {
    label: LabelType | null;
    labels: LabelType[];
    setLabel: (label: LabelType | null) => void;
    isLoadingLabels?: boolean;
}

export const LabelSelector: FC<LabelSelectorProps> = ({
    label,
    setLabel,
    labels,
    isLoadingLabels,
}) => {
    const [newLabelValue, setNewLabelValue] = useState<string>('');
    const [newLabels, setNewLabels] = useState<LabelType[]>([]);
    const [error, setError] = useState<string | null>(null);

    const [menuBtnElement, setMenuBtnElement] =
        useState<HTMLButtonElement | null>(null);
    const [menuElement, setMenuElement] = useState<HTMLDivElement | null>(null);

    const { styles, attributes } = usePopper(menuBtnElement, menuElement, {
        placement: 'bottom-start',
        modifiers: [{ name: 'offset', options: { offset: [0, 8] } }],
    });

    const isNoLabels =
        labels.length + newLabels.length === 0 && !isLoadingLabels;
    const isLabelsList = !isNoLabels && !isLoadingLabels;

    const handleAddNewLabel = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        setError(null);

        try {
            validateNewLabel(labels, newLabels, newLabelValue);
            const newLabel = getNewLabel(labels, newLabels, newLabelValue);
            setNewLabels(state => [...state, newLabel]);
            setNewLabelValue('');
        } catch (e) {
            setError(getGenericErrorMessage(e, _t.errorDataInput));
        }
    };

    const renderLabelsList = (labels: LabelType[], close: () => void) =>
        labels?.map(label => (
            <MenuButton
                key={label.color + label.value}
                hoverNoColor
                onClick={() => {
                    setLabel(label);
                    close();
                }}
            >
                <div
                    className="h-4 w-6 rounded-full"
                    style={{ background: `${label.color}` }}
                />
                <Text>{label.value}</Text>
            </MenuButton>
        ));

    return (
        <div className="flex flex-wrap items-center justify-between gap-4">
            <Popover>
                <Popover.Button as={Fragment}>
                    <Button
                        ref={setMenuBtnElement}
                        className="btn-primary btn-outline"
                    >
                        {_t.btnSelectLabel}
                    </Button>
                </Popover.Button>

                <MenuTransition>
                    <Popover.Panel
                        ref={setMenuElement}
                        style={styles.popper}
                        className="z-10 w-72 rounded-lg border border-base-content border-opacity-20 bg-base-100 p-2 shadow-2xl focus:outline-none"
                        {...attributes.popper}
                    >
                        {({ close }) => (
                            <>
                                <form className="relative w-full">
                                    <Input
                                        type="text"
                                        value={newLabelValue}
                                        onChange={e =>
                                            setNewLabelValue(e.target.value)
                                        }
                                        placeholder={_t.plcdAddLabel}
                                        className="pr-10"
                                        error={error}
                                    >
                                        <IconButton
                                            onClick={e => handleAddNewLabel(e)}
                                            type="submit"
                                            className="absolute right-0"
                                        >
                                            <PlusIcon />
                                        </IconButton>
                                    </Input>
                                </form>

                                {labels?.length + newLabels?.length > 0 && (
                                    <hr className="mb-2 mt-4 border-t border-t-base-content border-opacity-20 " />
                                )}

                                <div className="max-h-44 overflow-y-scroll">
                                    {isLoadingLabels && (
                                        <div className="flex flex-col items-stretch gap-3 p-3">
                                            <SkeletonList length={3} />
                                        </div>
                                    )}

                                    {isNoLabels && (
                                        <div className="flex justify-center p-3">
                                            <Text>{_t.noLabels}</Text>
                                        </div>
                                    )}

                                    {isLabelsList && (
                                        <>
                                            {renderLabelsList(
                                                newLabels,
                                                close
                                            )?.reverse()}
                                            {renderLabelsList(labels, close)}
                                        </>
                                    )}
                                </div>
                            </>
                        )}
                    </Popover.Panel>
                </MenuTransition>
            </Popover>

            {label && <Label label={label} onClose={() => setLabel(null)} />}
        </div>
    );
};
