import type { FC } from 'react';

import { _t, FormLabel } from '~/components';

export interface NotesInputProps {
    notes: string;
    setNotes: (notes: string) => void;
}

export const NotesInput: FC<NotesInputProps> = ({ notes, setNotes }) => (
    <div className="form-control">
        <FormLabel text={_t.notes} />
        <textarea
            className="textarea textarea-bordered h-20 hover:border-opacity-40 focus:border-primary focus:outline-1 focus:outline-offset-0 focus:outline-primary"
            value={notes}
            onChange={e => setNotes(e.target.value)}
        />
    </div>
);
