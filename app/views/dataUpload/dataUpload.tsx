import { useState } from 'react';
import { useNavigate } from '@remix-run/react';

import { FormsView, UploadPreviewPanel } from '..';

import { _t, Modal, ModalHeader } from '~/components';
import type { LabelType, Workout, WorkoutPreview } from '~/interfaces';

interface Props {
    labels: LabelType[];
    workout?: Workout;
}

export const DataUploadView: React.FC<Props> = ({ workout, labels }) => {
    const [previewData, setPreviewData] = useState<WorkoutPreview | null>(null);
    const navigate = useNavigate();

    return (
        <Modal isOpen={true} full>
            <div className="relative flex flex-col gap-4">
                <ModalHeader onClose={() => navigate(-1)}>
                    {previewData ? _t.dataPreviewHeader : _t.dataUploadHeader}
                </ModalHeader>

                {previewData ? (
                    <UploadPreviewPanel
                        workoutPreview={previewData}
                        setPreviewData={setPreviewData}
                    />
                ) : (
                    <FormsView
                        editWorkout={workout}
                        labels={labels}
                        setPreviewData={setPreviewData}
                    />
                )}
            </div>
        </Modal>
    );
};
