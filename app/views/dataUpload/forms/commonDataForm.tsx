import type { FC } from 'react';
import { useEffect } from 'react';

import type { NotesInputProps } from '../components';
import { LabelSelector, NotesInput } from '../components';

import { _t } from '~/components';
import type { LabelType, Workout } from '~/interfaces';

interface Props extends NotesInputProps {
    editWorkout?: Workout | null;
    labels: LabelType[];
    label: LabelType | null;
    setLabel: (label: LabelType | null) => void;
}

export const CommonDataForm: FC<Props> = ({
    label,
    setLabel,
    labels,
    notes,
    setNotes,
    editWorkout,
}) => {
    useEffect(() => {
        if (editWorkout?.label) setLabel(editWorkout.label);
        if (editWorkout?.notes) setNotes(editWorkout.notes);
    }, []);

    return (
        <div className="flex flex-col items-stretch gap-4">
            <LabelSelector setLabel={setLabel} label={label} labels={labels} />

            <NotesInput notes={notes} setNotes={setNotes} />
        </div>
    );
};
