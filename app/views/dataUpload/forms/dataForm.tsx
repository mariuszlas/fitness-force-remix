import type { FC } from 'react';

import type {
    ActivitySelectorProps,
    DatetimePickerProps,
    DistanceInputProps,
    DurationPickerProps,
    TimezoneSelectorProps,
} from '../components';
import {
    ActivitySelector,
    DatetimePicker,
    DistanceInput,
    DurationPicker,
    TimezoneSelector,
} from '../components';

type Props = ActivitySelectorProps &
    DistanceInputProps &
    DurationPickerProps &
    DatetimePickerProps &
    TimezoneSelectorProps;

export const DataUploadForm: FC<Props> = ({
    type,
    setType,
    distance,
    setDistance,
    duration,
    setDuration,
    timestamp,
    setTimestamp,
    utcOffset,
    setUtcOffset,
}) => (
    <div className="flex flex-wrap justify-between gap-2 py-4">
        <ActivitySelector type={type} setType={setType} />
        <DistanceInput distance={distance} setDistance={setDistance} />
        <DurationPicker duration={duration} setDuration={setDuration} />
        <DatetimePicker timestamp={timestamp} setTimestamp={setTimestamp} />
        <TimezoneSelector utcOffset={utcOffset} setUtcOffset={setUtcOffset} />
    </div>
);
