import type { FC } from 'react';

import type { FilePickerProps } from '../components';
import { FilePicker } from '../components';

import { _t, InformationIcon, Text } from '~/components';

export const FileUpload: FC<FilePickerProps> = props => (
    <div className="flex flex-col items-stretch gap-4 py-4">
        <FilePicker {...props} />
        <div className="flex items-center gap-2 rounded-lg border-l-4 border-l-blue-600 bg-blue-100 p-2">
            <InformationIcon className="h-10 w-10 text-blue-600" />
            <Text className="text-info-content">{_t.fileUploadInfo}</Text>
        </div>
    </div>
);
