import type { FC, FormEvent } from 'react';
import { Fragment, useEffect, useState } from 'react';
import { Tab } from '@headlessui/react';
import { Form, useFetcher } from '@remix-run/react';

import { CommonDataForm, DataUploadForm, FileUpload } from './forms';
import { formatAndValidateData } from './helpers';

import { _t, Button } from '~/components';
import type { LabelType, Workout, WorkoutPreview } from '~/interfaces';
import { WorkoutTypes } from '~/interfaces';
import { cn } from '~/utils/helpers';

const FORM_ID = 'workout-upload-form';

interface Props {
    editWorkout?: Workout;
    labels: LabelType[];
    setPreviewData: (data: WorkoutPreview | null) => void;
}

export const FormsView: FC<Props> = ({
    editWorkout,
    labels,
    setPreviewData,
}) => {
    const fetcher = useFetcher();
    const isLoading =
        fetcher.state === 'loading' || fetcher.state === 'submitting';

    const [isDisabled, setIsDisabled] = useState(false);
    const [file, setFile] = useState<File | null>(null);
    const [duration, setDuration] = useState<number>(
        editWorkout?.duration || 0
    );
    const [coordinates, setCoordinates] = useState<[number, number][]>([]);
    const [label, setLabel] = useState<LabelType | null>(null);
    const [notes, setNotes] = useState('');
    const [timestamp, setTimestamp] = useState(editWorkout?.timestamp || '');
    const [utcOffset, setUtcOffset] = useState(
        editWorkout?.utcOffset || new Date().getTimezoneOffset() / -60
    );
    const [distance, setDistance] = useState(
        editWorkout?.distance.toString() || ''
    );
    const [type, setType] = useState(
        editWorkout?.type || (WorkoutTypes.RUNNING as string)
    );

    useEffect(() => {
        if (fetcher.data) {
            setPreviewData(fetcher.data);
        }
    }, [fetcher.data, setPreviewData]);

    const handleRequestDataPreview = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            const workoutData = formatAndValidateData(
                type,
                timestamp,
                utcOffset,
                distance,
                duration,
                label,
                notes,
                coordinates
            );

            fetcher.submit(
                { workoutData: JSON.stringify(workoutData) },
                { method: 'POST' }
            );
        } catch (err) {}
    };

    return (
        <>
            <Form
                method="POST"
                id={FORM_ID}
                onSubmit={e => handleRequestDataPreview(e)}
            >
                <Tab.Group defaultIndex={editWorkout ? 1 : 0}>
                    <Tab.List className="tabs">
                        <Tab as={Fragment}>
                            {({ selected }) => (
                                <button
                                    disabled={!!editWorkout}
                                    className={cn(
                                        'tab tab-bordered h-full w-1/2 p-2 text-base font-semibold',
                                        {
                                            'tab-active text-primary': selected,
                                        }
                                    )}
                                >
                                    {_t.btnUploadFile}
                                </button>
                            )}
                        </Tab>
                        <Tab as={Fragment}>
                            {({ selected }) => (
                                <button
                                    className={cn(
                                        'tab tab-bordered h-full w-1/2 p-2 text-base font-semibold',
                                        {
                                            'tab-active text-primary': selected,
                                        }
                                    )}
                                >
                                    {editWorkout
                                        ? _t.btnEditWorkout
                                        : _t.btnAddData}
                                </button>
                            )}
                        </Tab>
                    </Tab.List>

                    <Tab.Panels>
                        <Tab.Panel>
                            <FileUpload
                                {...{
                                    setDistance,
                                    setTimestamp,
                                    setUtcOffset,
                                    setDuration,
                                    setType,
                                    setCoordinates,
                                    file,
                                    setFile,
                                }}
                            />
                        </Tab.Panel>

                        <Tab.Panel>
                            <DataUploadForm
                                {...{
                                    timestamp,
                                    setTimestamp,
                                    utcOffset,
                                    setUtcOffset,
                                    distance,
                                    setDistance,
                                    duration,
                                    setDuration,
                                    setIsDisabled,
                                    type,
                                    setType,
                                }}
                            />
                        </Tab.Panel>
                    </Tab.Panels>
                </Tab.Group>

                <CommonDataForm
                    {...{
                        label,
                        setLabel,
                        labels,
                        notes,
                        setNotes,
                        editWorkout,
                    }}
                />
            </Form>

            <Button
                disabled={isDisabled || isLoading}
                className="btn-primary btn-block"
                type="submit"
                form={FORM_ID}
                isLoading={isLoading}
            >
                {_t.btnUpload}
            </Button>
        </>
    );
};
