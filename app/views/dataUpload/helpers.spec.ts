import {
    formatAndValidateData,
    formatPreviewItem,
    formatPreviewMessage,
    getTypeFromFilename,
    validateType,
} from './helpers';

import { _t } from '~/constants';
import type { LabelType, UploadWorkout, Workout } from '~/interfaces';
import { WorkoutTypes } from '~/interfaces';

describe('web/view/dataUpload/views/helpers', () => {
    describe('getTypeFromFilename', () => {
        it('should throw an error if fileName is undefined', () => {
            expect(() => getTypeFromFilename(undefined)).toThrow(
                _t.errorParsingFile
            );
        });

        it("should return WorkoutTypes.WALKING if fileName contains 'Walk'", () => {
            const fileName = 'Walk.tcx';
            const actual = getTypeFromFilename(fileName);

            expect(actual).toEqual(WorkoutTypes.WALKING);
        });

        it("should throw an error if fileName does not contain 'Walk'", () => {
            const fileName = 'Run.tcx';

            expect(() => getTypeFromFilename(fileName)).toThrow(
                _t.errorInvActivType
            );
        });
    });

    describe('validateType', () => {
        const fileName = 'Walk.gpx';

        it('should throw an error if sportType is undefined', () => {
            expect(() => validateType(undefined)).toThrow(_t.errorParsingFile);
        });

        it('should return the sportType if it is a valid value', () => {
            const sportType = WorkoutTypes.RUNNING;

            const actual = validateType(sportType, fileName);
            expect(actual).toEqual(sportType);

            const actualNoFileName = validateType(sportType);
            expect(actualNoFileName).toEqual(sportType);
        });

        it('should return the type from the fileName if sportType is not a valid value', () => {
            const actual = validateType('not-valid', fileName);
            expect(actual).toEqual(WorkoutTypes.WALKING);
        });
    });

    describe('formatAndValidateData', () => {
        let activity_type: string;
        let timestamp: string;
        let utcOffset: number;
        let distance: number;
        let duration: number;
        let label: LabelType | null;
        let notes: string | null;
        let coordinates: number[][];

        beforeEach(() => {
            activity_type = 'running';
            timestamp = '2023-03-08T12:00:00Z';
            utcOffset = 0;
            distance = 10;
            duration = 60;
            label = null;
            notes = null;
            coordinates = [[]];
        });

        it('should throw an error if duration is not provided', () => {
            expect(() =>
                formatAndValidateData(
                    activity_type,
                    timestamp,
                    utcOffset,
                    distance,
                    undefined,
                    label,
                    notes,
                    coordinates
                )
            ).toThrow(_t.errorDataInput);
        });

        it('should throw an error if distance is less than 0.5', () => {
            distance = 0.4;

            expect(() =>
                formatAndValidateData(
                    activity_type,
                    timestamp,
                    utcOffset,
                    distance,
                    duration,
                    label,
                    notes,
                    coordinates
                )
            ).toThrow(_t.errorMinDistance);
        });

        it('should return the workout object if the data is valid', () => {
            label = { value: 'My Label', color: 'blue' };
            notes = 'Some notes';
            coordinates = [
                [1, 2],
                [3, 4],
            ];

            const actual = formatAndValidateData(
                activity_type,
                timestamp,
                utcOffset,
                distance,
                duration,
                label,
                notes,
                coordinates
            );

            expect(actual).toEqual({
                type: WorkoutTypes.RUNNING,
                distance,
                timestamp,
                duration,
                label,
                notes,
                geolocation: coordinates,
                utcOffset: utcOffset,
            });
        });

        it('should return the workout object if the data is valid (with distance as a string)', () => {
            label = { value: 'My Label', color: 'blue' };
            notes = 'Some notes';
            coordinates = [
                [1, 2],
                [3, 4],
            ];

            const actual = formatAndValidateData(
                activity_type,
                timestamp,
                utcOffset,
                distance.toString(),
                duration,
                label,
                notes,
                coordinates
            );

            expect(actual).toEqual({
                type: WorkoutTypes.RUNNING,
                distance,
                timestamp,
                duration,
                label,
                notes,
                geolocation: coordinates,
                utcOffset: utcOffset,
            });
        });

        it('should return the workout object if the data is valid (no coordinates)', () => {
            label = { value: 'My Label', color: 'blue' };
            notes = 'Some notes';
            coordinates = [];

            const actual = formatAndValidateData(
                activity_type,
                timestamp,
                utcOffset,
                distance.toString(),
                duration,
                label,
                notes,
                coordinates
            );

            expect(actual).toEqual({
                type: WorkoutTypes.RUNNING,
                distance,
                timestamp,
                duration,
                label,
                notes,
                geolocation: null,
                utcOffset: utcOffset,
            });
        });
    });

    describe('formatPreviewMessage', () => {
        const workout = {
            type: 'running',
            timestamp: '2023-03-08T12:00:00Z',
            utcOffset: 1,
        } as unknown as UploadWorkout;

        beforeAll(() => {
            const origDate = global.Date.prototype.toLocaleDateString;
            jest.spyOn(
                global.Date.prototype,
                'toLocaleDateString'
                //eslint-disable-next-line @typescript-eslint/no-explicit-any
            ).mockImplementation(function (this: any) {
                return origDate.call(this, 'en-GB');
            });
        });

        it('should return the correct message if there are no existing data records', () => {
            const existingData: Workout[] = [];
            const actual = formatPreviewMessage(existingData, workout);

            expect(actual).toEqual(
                'There are no records for running on 08/03/2023, 13:00'
            );
        });

        it('should return the correct message if there is one existing data record', () => {
            const existingData = [{ type: 'running' }] as unknown as Workout[];
            const actual = formatPreviewMessage(existingData, workout);

            expect(actual).toEqual(
                'There is 1 record for running on 08/03/2023, 13:00'
            );
        });

        it('should return the correct message if there are multiple existing data records', () => {
            const existingData = [
                { type: 'running' },
                { type: 'running' },
            ] as unknown as Workout[];
            const actual = formatPreviewMessage(existingData, workout);

            expect(actual).toEqual(
                'There are 2 records for running on 08/03/2023, 13:00'
            );
        });
    });

    describe('formatPreviewItem', () => {
        it('should return the correct message with distance and duration', () => {
            const workout = {
                distance: 10,
                duration: 60,
            } as unknown as Workout;
            const actual = formatPreviewItem(workout);

            expect(actual).toEqual('Distance: 10.0 km, Duration: 00:01:00');
        });
    });
});
