import { getDateTimeTZ, getDuration } from '../helpers';

import { _t } from '~/constants';
import type { LabelType, UploadWorkout, Workout } from '~/interfaces';
import { WorkoutTypes } from '~/interfaces';

export const getTypeFromFilename = (fileName: string | undefined) => {
    if (!fileName) throw Error(_t.errorParsingFile);

    if (/Walk/.test(fileName)) return WorkoutTypes.WALKING;
    throw Error(_t.errorInvActivType);
};

export const validateType = (
    sportType: string | undefined,
    fileName?: string
) => {
    if (!sportType) throw Error(_t.errorParsingFile);
    const allTypes = Object.values(WorkoutTypes);

    if (allTypes.includes(sportType as WorkoutTypes)) {
        return sportType as WorkoutTypes;
    }
    return getTypeFromFilename(fileName);
};

export const formatAndValidateData = (
    activity_type: string,
    timestamp: string,
    utcOffset: number,
    distance: string | number,
    duration: number | undefined,
    label: LabelType | null,
    notes: string | null,
    coordinates: number[][]
): UploadWorkout => {
    if (!duration) throw Error(_t.errorDataInput);

    if (typeof distance === 'string') distance = parseFloat(distance);

    if (distance < 0.5) throw Error(_t.errorMinDistance);

    const type = validateType(activity_type);

    return {
        type,
        distance,
        timestamp,
        duration,
        label: label,
        notes: notes,
        geolocation: coordinates?.length ? coordinates : null,
        utcOffset: utcOffset,
    };
};

export const formatPreviewMessage = (
    existingData: Workout[] | undefined,
    workout: UploadWorkout
) => {
    const dataRecords = existingData?.length || 'no';
    const isSingular = dataRecords === 1;
    const date = getDateTimeTZ(workout.timestamp, workout.utcOffset);

    return `${isSingular ? _t.previewMsgIs : _t.previewMsgAre} ${dataRecords} ${
        isSingular ? _t.previewMsgRecord : _t.previewMsgRecordPlural
    } for ${workout.type} ${_t.previewMsgOn} ${date}`;
};

export const formatPreviewItem = (workout: UploadWorkout | Workout) =>
    `${_t.distance}: ${workout.distance.toFixed(1)} ${_t.km}, ${
        _t.duration
    }: ${getDuration(Number(workout.duration))}`;
