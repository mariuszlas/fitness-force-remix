import { type FC, useEffect } from 'react';
import { useFetcher, useNavigate } from '@remix-run/react';

import { formatPreviewItem, formatPreviewMessage } from './helpers';

import { _t, Alert, Button, Heading, Text } from '~/components';
import type { WorkoutPreview } from '~/interfaces';

interface Props {
    workoutPreview: WorkoutPreview;
    setPreviewData: (data: WorkoutPreview | null) => void;
    editWorkout?: boolean;
}

export const UploadPreviewPanel: FC<Props> = ({
    editWorkout,
    workoutPreview,
    setPreviewData,
}) => {
    const fetcher = useFetcher<{ success: boolean }>();
    const navigate = useNavigate();
    const isLoading =
        fetcher.state === 'loading' || fetcher.state === 'submitting';

    const existingData = workoutPreview?.foundData;
    const dataToUpload = workoutPreview?.data;
    const isExistingData = existingData && existingData.length > 0;

    const closeDataPreview = () => {
        if (editWorkout) {
            navigate(-1);
        } else {
            setPreviewData(null);
        }
    };

    useEffect(() => {
        if (fetcher.data?.success) {
            setPreviewData(null);
        }
    }, [fetcher.data, setPreviewData]);

    const uploadWorkout = () => {
        if (!workoutPreview || !dataToUpload) return;

        fetcher.submit(
            { final: true, workoutData: JSON.stringify(dataToUpload) },
            { method: 'POST' }
        );
    };

    return (
        <>
            <div className="flex flex-col items-stretch gap-4">
                <Alert
                    status={isExistingData ? 'warning' : 'success'}
                    classes="m-0 p-2"
                >
                    {formatPreviewMessage(existingData, dataToUpload)}
                </Alert>

                {isExistingData && (
                    <div data-testid="found-data-section">
                        <Heading as="h3">{_t.foundData}</Heading>
                        <ul>
                            {existingData.map(workout => (
                                <li key={workout.id}>
                                    {formatPreviewItem(workout)}
                                </li>
                            ))}
                        </ul>
                    </div>
                )}

                <div data-testid="data-to-upload-section">
                    <Heading as="h3">{_t.dataToUpload}</Heading>
                    <Text>{formatPreviewItem(dataToUpload)}</Text>
                </div>
            </div>

            <div className="flex justify-end gap-4">
                <Button
                    className="btn-ghost"
                    onClick={() => closeDataPreview()}
                >
                    {_t.btnCancel}
                </Button>
                <Button
                    className="btn-primary"
                    isLoading={isLoading}
                    onClick={() => uploadWorkout()}
                >
                    {_t.btnUpload}
                </Button>
            </div>
        </>
    );
};
