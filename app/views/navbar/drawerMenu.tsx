import type { FC } from 'react';

import { WorkoutMenu } from '..';

import {
    _t,
    CloseButton,
    Drawer,
    MenuLink,
    PlusIcon,
    Text,
    ThemeSwitch,
} from '~/components';

interface Props {
    isProtected: boolean;
    isDrawerOpen: boolean;
    setIsDrawerOpen: (isDrawerOpen: boolean) => void;
    isSignupPage: boolean;
}

export const DrawerMenu: FC<Props> = ({
    isProtected,
    isDrawerOpen,
    setIsDrawerOpen,
    isSignupPage,
}) => (
    <Drawer isOpen={isDrawerOpen} setIsOpen={setIsDrawerOpen}>
        <div className="flex w-full items-center justify-between p-4">
            <Text className="text-xl font-semibold">{_t.mainMenu}</Text>
            <CloseButton onClick={() => setIsDrawerOpen(false)} />
        </div>

        <hr className="border-t border-t-base-content border-opacity-20 " />

        <ul className="w-full p-4">
            <ThemeSwitch isMobile />

            {isProtected && (
                <>
                    <li>
                        <MenuLink
                            aria-label="add workout"
                            to="new"
                            relative="path"
                            onClick={() => setIsDrawerOpen(false)}
                        >
                            <Text>{_t.btnAddWorkout}</Text>
                            <PlusIcon />
                        </MenuLink>
                    </li>

                    <hr className="my-4 border-t border-t-base-content border-opacity-20 " />

                    <WorkoutMenu
                        isMobile
                        onClose={() => setIsDrawerOpen(false)}
                    />
                </>
            )}

            {!isProtected && !isSignupPage && (
                <li>
                    <MenuLink
                        to="/signup"
                        onClick={() => setIsDrawerOpen(false)}
                    >
                        {_t.btnSignup}
                    </MenuLink>
                </li>
            )}
        </ul>
    </Drawer>
);
