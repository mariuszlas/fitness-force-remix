import type { FC } from 'react';
import { useState } from 'react';
import { Link, useMatches } from '@remix-run/react';

import {
    _t,
    BurgerMenuIcon,
    Logo,
    PlusIcon,
    Text,
    ThemeSwitch,
} from '../../components';
import { AccountMenu, WorkoutMenu } from '..';

import { DrawerMenu } from './drawerMenu';
import { FloatingNewWorkoutBtn } from './floatingButton';

interface NavBarProps {
    isProtected: boolean;
}

export const NavBar: FC<NavBarProps> = ({ isProtected }) => {
    const [isDrawerOpen, setIsDrawerOpen] = useState(false);
    const matches = useMatches();

    const isLoginPage = matches.at(3)?.pathname === '/login';
    const isSignupPage = matches.at(3)?.pathname === '/signup';
    const isEmailVerificationPage = matches.at(3)?.pathname === '/verify';

    const showLoginBtn = !(isLoginPage || isEmailVerificationPage);
    const showSignupBtn = !isSignupPage;

    return (
        <>
            <header className="sticky top-0 z-50 w-full border-b border-b-base-content border-opacity-20 bg-base-100 bg-opacity-30 py-2 shadow-sm backdrop-blur-lg">
                <nav className="mx-auto flex w-full max-w-8xl items-center px-4 sm:px-8">
                    <div className="navbar-start">
                        <div className="hidden md:block">
                            <Logo isProtected={isProtected} />
                        </div>

                        <div className="md:hidden">
                            <button
                                onClick={() => setIsDrawerOpen(true)}
                                className="btn btn-ghost h-10 min-h-min"
                                type="button"
                            >
                                <BurgerMenuIcon />
                            </button>
                        </div>
                    </div>

                    <div className="navbar-center">
                        <div className="md:hidden">
                            <Logo isProtected={isProtected} />
                        </div>

                        {isProtected && (
                            <div className="hidden gap-4 md:flex">
                                <WorkoutMenu />

                                <Link
                                    aria-label="add workout"
                                    className="btn btn-primary btn-outline h-10 min-h-min"
                                    to="new"
                                    relative="path"
                                >
                                    <Text>{_t.btnAddWorkout}</Text>
                                    <PlusIcon />
                                </Link>
                            </div>
                        )}
                    </div>

                    <div className="navbar-end flex items-center gap-4">
                        <ThemeSwitch />

                        {isProtected ? (
                            <AccountMenu />
                        ) : (
                            <>
                                {showLoginBtn && (
                                    <Link
                                        className="btn btn-primary btn-outline h-10 min-h-min"
                                        to="/login"
                                        color="primary"
                                    >
                                        {_t.btnLogin}
                                    </Link>
                                )}

                                {showSignupBtn && (
                                    <Link
                                        className="btn btn-primary hidden h-10 min-h-min sm:inline-flex"
                                        to="/signup"
                                        color="primary"
                                    >
                                        {_t.btnSignup}
                                    </Link>
                                )}
                            </>
                        )}
                    </div>
                </nav>
            </header>

            <DrawerMenu
                isDrawerOpen={isDrawerOpen}
                setIsDrawerOpen={setIsDrawerOpen}
                isProtected={isProtected}
                isSignupPage={isSignupPage}
            />

            {isProtected && <FloatingNewWorkoutBtn />}
        </>
    );
};
