import type { FC } from 'react';
import { useNavigation } from '@remix-run/react';

import { StatsPanel } from './statsPanel/statsPanel';
import { selectPmStatsData, selectSecStatsData } from './helpers';

import { useIsBreakpoint } from '~/hooks';
import type { WorkoutsDashboard } from '~/interfaces';
import { useUI } from '~/providers';

interface Props {
    dashboard: WorkoutsDashboard | undefined;
    isLoading?: boolean;
}

export const StatisticsView: FC<Props> = ({ dashboard, isLoading }) => {
    const isMobileOrTabled = useIsBreakpoint('md');

    const navigation = useNavigation();
    const isInitLoading =
        (navigation.state === 'loading' &&
            navigation.location.pathname.split('/').length === 2) ||
        isLoading;

    const { year, secondaryStat } = useUI();
    const headerData = { year, secStats: secondaryStat };

    const isError = false;

    const pmStatsData = selectPmStatsData(dashboard, headerData.year);

    const secStatsData = selectSecStatsData(dashboard, headerData);

    return (
        <div className="mt-4 grid w-full grid-cols-2 gap-6">
            <StatsPanel
                data={pmStatsData}
                headerData={headerData}
                isPrimary={true}
                isError={isError}
                isLoading={isInitLoading}
            />

            <StatsPanel
                data={secStatsData}
                headerData={headerData}
                isPrimary={false}
                isMobile={isMobileOrTabled}
                isError={isError}
                isLoading={isInitLoading}
            />
        </div>
    );
};
