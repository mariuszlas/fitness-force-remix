import type { FC } from 'react';

import { getPace } from '../../helpers';

import { getDecimal, getStatsPanelHeading } from './helpers';
import { StatsPanelEntry } from './statsPanelEntry';

import { _t, Alert, Heading } from '~/components';
import type { MonthStats, TotalStats, YearStats } from '~/interfaces';
import { Category } from '~/interfaces';

interface Props {
    data: TotalStats | YearStats | MonthStats | undefined;
    headerData: { year: number; secStats: number };
    isPrimary: boolean;
    isError: boolean;
    isMobile?: boolean;
    isLoading?: boolean;
}

export const StatsPanel: FC<Props> = ({
    data,
    headerData,
    isPrimary,
    isError,
    isMobile,
    isLoading,
}) => (
    <section
        className="flex flex-col items-start gap-2 border-base-content border-opacity-20 p-4 sm:gap-4 sm:rounded-xl sm:border sm:p-6 sm:shadow-lg"
        data-testid={`${isPrimary ? 'primary' : 'secondary'}-stats-section`}
    >
        <header>
            <Heading
                as="h2"
                title={`${
                    isPrimary ? 'primary' : 'secondary'
                }-stats-section-title`}
            >
                {getStatsPanelHeading(isPrimary, headerData, isMobile)}
            </Heading>
        </header>

        {!isError ? (
            <div className="flex w-full flex-col gap-3">
                <StatsPanelEntry
                    data={data?.distance?.toFixed(1) ?? 0}
                    category={Category.DISTANCE}
                    units={_t.km}
                    icon="road"
                    isLoading={isLoading}
                />

                <StatsPanelEntry
                    data={getDecimal(data?.duration)?.time ?? 0}
                    category={Category.DURATION}
                    units={getDecimal(data?.duration)?.unit ?? _t.h}
                    icon="clockCircle"
                    isLoading={isLoading}
                />

                <StatsPanelEntry
                    data={getPace(data?.duration, data?.distance) ?? 0}
                    category={Category.PACE}
                    units={_t.perKm}
                    icon="speedometer"
                    isLoading={isLoading}
                />

                <StatsPanelEntry
                    data={data?.counts ?? 0}
                    category={Category.EX_TIMES}
                    units={''}
                    icon="counter"
                    isLoading={isLoading}
                />
            </div>
        ) : (
            <Alert status="error">{_t.errorFetch}</Alert>
        )}
    </section>
);
