import {
    ClockIcon,
    CounterIcon,
    RoadIcon,
    Skeleton,
    SpeedeometerIcon,
    Text,
} from '~/components';
import type { Category, StatIconType } from '~/interfaces';

export const getStatIcon = (iconType: StatIconType, isLineItem?: boolean) => {
    const classes = isLineItem
        ? 'h-5 w-5 text-primary'
        : 'h-8 w-8 text-primary';

    switch (iconType) {
        case 'road':
            return <RoadIcon className={classes} />;
        case 'clockCircle':
            return <ClockIcon className={classes} />;
        case 'speedometer':
            return <SpeedeometerIcon className={classes} />;
        case 'counter':
            return <CounterIcon className={classes} />;
        default:
            return null;
    }
};

interface Props {
    data: number | string | undefined;
    category: Category;
    units: string;
    icon: StatIconType;
    isLoading?: boolean;
}

export const StatsPanelEntry: React.FC<Props> = ({
    data,
    category,
    units,
    icon,
    isLoading,
}) => {
    return (
        <>
            {isLoading ? (
                <Skeleton h={12} />
            ) : (
                <div className="flex items-center gap-3">
                    {getStatIcon(icon)}

                    <div className="flex flex-col">
                        <Text>{category}</Text>

                        <div>
                            <Text className="font-medium">{data}</Text>
                            <Text>{` ${units}`}</Text>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};
