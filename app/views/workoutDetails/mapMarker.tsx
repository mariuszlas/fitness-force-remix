import { MapPinIcon } from '~/components';
import { cn } from '~/utils/helpers';

export enum MarkerTypes {
    START = 'start',
    FINISH = 'finish',
}

interface Props {
    lat: number;
    lng: number;
    type: MarkerTypes;
}

export const Marker: React.FC<Props> = ({ type }) => (
    <MapPinIcon
        className={cn(
            'absolute h-8 w-8 -translate-x-1/2 -translate-y-full transform',
            type === MarkerTypes.START ? 'text-success' : 'text-error'
        )}
    />
);
