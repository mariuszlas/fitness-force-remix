import type { Workout, WorkoutTypes } from '~/interfaces';

export interface Props {
    data: Workout;
}

export interface TypeProps extends Props {
    type: WorkoutTypes;
}
