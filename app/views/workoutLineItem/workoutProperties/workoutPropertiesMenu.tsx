import type { FC } from 'react';
import { Fragment, useState } from 'react';
import { usePopper } from 'react-popper';
import { Menu } from '@headlessui/react';

import type { Props } from '../interfaces';

import {
    _t,
    EditIcon,
    IconButton,
    InformationIcon,
    MenuLink,
    MenuTransition,
    MoreVerticalIcon,
    Text,
    TrashIcon,
} from '~/components';
import type { Workout } from '~/interfaces';
import { cn } from '~/utils/helpers';

export const WorkoutPropertiesMenu: FC<Props> = ({ data }) => {
    const [menuBtnElement, setMenuBtnElement] =
        useState<HTMLButtonElement | null>(null);
    const [menuElement, setMenuElement] = useState<HTMLDivElement | null>(null);

    const { styles, attributes } = usePopper(menuBtnElement, menuElement, {
        placement: 'bottom-end',
        modifiers: [{ name: 'offset', options: { offset: [0, 8] } }],
    });

    const items = [
        {
            getHref: (id: number) => `${id}`,
            text: _t.workoutDetails,
            icon: <InformationIcon />,
            getContext: (workout: Workout) => ({
                isLarge: workout.hasTrajectory,
            }),
        },
        {
            getHref: (id: number) => `edit/${id}`,
            text: _t.edit,
            icon: <EditIcon />,
        },
        {
            getHref: (id: number) => `delete/${id}`,
            text: _t.delete,
            icon: <TrashIcon />,
            hoverRed: true,
        },
    ];

    return (
        <Menu as="div">
            <Menu.Button as={Fragment}>
                <IconButton
                    ref={setMenuBtnElement}
                    aria-label={`workout ${data.id} properties menu`}
                >
                    <MoreVerticalIcon />
                </IconButton>
            </Menu.Button>

            <MenuTransition>
                <Menu.Items
                    ref={setMenuElement}
                    style={styles.popper}
                    className={cn(
                        'z-10 w-52 rounded-lg border border-base-content border-opacity-20 bg-base-100 p-2 shadow-2xl focus:outline-none'
                    )}
                    {...attributes.popper}
                >
                    {items.map(
                        ({ getHref, text, icon, hoverRed, getContext }) => (
                            <Menu.Item key={text}>
                                {({ active }) => (
                                    <MenuLink
                                        active={active}
                                        danger={hoverRed}
                                        state={getContext && getContext(data)}
                                        to={getHref(data.id)}
                                    >
                                        {icon}
                                        <Text>{text}</Text>
                                    </MenuLink>
                                )}
                            </Menu.Item>
                        )
                    )}
                </Menu.Items>
            </MenuTransition>
        </Menu>
    );
};
