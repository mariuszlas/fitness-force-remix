import { handleSort } from './helpers';

describe('web/views/workoutList/components/helpers', () => {
    describe('handleSort', () => {
        it('should set the sort to the selected value', () => {
            const e = {
                target: { value: 'name-reverse' },
                preventDefault: jest.fn(),
            } as unknown as React.ChangeEvent<HTMLSelectElement>;
            const setSortBy = jest.fn();

            handleSort(e, setSortBy);

            expect(setSortBy).toBeCalledWith({
                sort: 'name',
                reverse: true,
            });
        });

        it("should set the reverse to false if the value does not contain 'reverse'", () => {
            const e = {
                target: { value: 'name' },
                preventDefault: jest.fn(),
            } as unknown as React.ChangeEvent<HTMLSelectElement>;
            const setSortBy = jest.fn();

            handleSort(e, setSortBy);

            expect(setSortBy).toBeCalledWith({
                sort: 'name',
                reverse: false,
            });
        });
    });
});
