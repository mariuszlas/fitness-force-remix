import type { FC } from 'react';
import { useEffect, useState } from 'react';
import { useMatches, useNavigation } from '@remix-run/react';

import { WorkoutListHeader } from './header';
import { filterWorkouts, selectMonthWorkouts } from './helpers';
import { WorkoutList } from './workoutsList';

import { SkeletonList } from '~/components';
import type { PagedWorkoutList, WorkoutTypes } from '~/interfaces';
import { useUI } from '~/providers';

interface Props {
    data: PagedWorkoutList | undefined;
    isInitLoading?: boolean;
}

export const WorkoutListView: FC<Props> = ({ data, isInitLoading }) => {
    const navigation = useNavigation();
    const matches = useMatches();
    const { year, secondaryStat } = useUI();

    const workoutType = matches.at(2)?.params?.workoutType as WorkoutTypes;
    const headerData = { year, secStats: secondaryStat };
    const allWorkouts = data?.content ?? [];
    const monthWorkouts = selectMonthWorkouts(data, headerData);

    const isLoading =
        (navigation.state === 'loading' &&
            navigation.location.pathname.split('/').length === 2) ||
        isInitLoading;

    const [sortBy, setSortBy] = useState({ sort: 'date', reverse: false });
    const [filterBy, setFilterBy] = useState('');
    const [isAll, setIsAll] = useState(false);
    const [pageNo, setPageNo] = useState(1);

    const workouts = isAll ? allWorkouts : monthWorkouts;
    const filterdWks = filterWorkouts(filterBy, workouts);

    useEffect(() => {
        setPageNo(1);
    }, [workoutType, year, secondaryStat]);

    return (
        <section
            className="flex flex-col items-start gap-2 border-base-content border-opacity-20 p-4 sm:gap-4 sm:rounded-xl sm:border sm:p-6 sm:shadow-lg lg:min-h-full"
            data-testid="workout-list-section"
        >
            <WorkoutListHeader
                headerData={headerData}
                setSortBy={setSortBy}
                filterBy={filterBy}
                setFilterBy={setFilterBy}
                isAll={isAll}
                setIsAll={setIsAll}
                setPageNo={setPageNo}
            />

            {isLoading ? (
                <SkeletonList length={3} />
            ) : (
                <WorkoutList
                    workoutType={workoutType}
                    sortBy={sortBy}
                    workouts={filterdWks}
                    pageNo={pageNo}
                    setPageNo={setPageNo}
                    isError={false}
                />
            )}
        </section>
    );
};
