import { Fragment, useState } from 'react';
import { usePopper } from 'react-popper';
import { Menu } from '@headlessui/react';
import { useMatches } from '@remix-run/react';

import {
    Button,
    ChevronDownIcon,
    MenuLink,
    MenuTransition,
    Text,
} from '~/components';
import { WorkoutTypes } from '~/interfaces';
import { cn } from '~/utils/helpers';
import { capitalize } from '~/views/helpers';

interface WorkoutMenuProps {
    onClose?: () => void;
    isMobile?: boolean;
}

export const WorkoutMenu: React.FC<WorkoutMenuProps> = ({
    onClose,
    isMobile,
}) => {
    const [menuBtnElement, setMenuBtnElement] =
        useState<HTMLButtonElement | null>(null);
    const [menuElement, setMenuElement] = useState<HTMLDivElement | null>(null);

    const { styles, attributes } = usePopper(menuBtnElement, menuElement, {
        placement: 'bottom-start',
        modifiers: [{ name: 'offset', options: { offset: [-2, 8] } }],
    });

    const matches = useMatches();

    const currentWorkoutType = matches.at(2)?.params
        ?.workoutType as WorkoutTypes;

    const workoutOptions = Object.values(WorkoutTypes).filter(
        workoutType => workoutType !== currentWorkoutType
    );

    if (isMobile) {
        return (
            <>
                {Object.values(WorkoutTypes).map(workoutType => (
                    <li key={workoutType.toString()}>
                        <MenuLink
                            to={`/dashboard/${workoutType}`}
                            onClick={() => onClose && onClose()}
                        >
                            {capitalize(workoutType)}
                        </MenuLink>
                    </li>
                ))}
            </>
        );
    }

    return (
        <Menu as="div">
            {({ open }) => (
                <>
                    <Menu.Button as={Fragment}>
                        <Button
                            className="btn-primary btn-outline"
                            aria-label={capitalize(currentWorkoutType)}
                            ref={setMenuBtnElement}
                        >
                            <Text>{capitalize(currentWorkoutType)}</Text>
                            <ChevronDownIcon
                                className={cn(
                                    'transform duration-300 ease-in-out',
                                    open ? 'rotate-180' : 'rotate-0'
                                )}
                            />
                        </Button>
                    </Menu.Button>

                    <MenuTransition>
                        <Menu.Items
                            ref={setMenuElement}
                            style={styles.popper}
                            className="z-10 w-52 rounded-lg border border-base-content border-opacity-20 bg-base-100 p-2 shadow-2xl focus:outline-none"
                            {...attributes.popper}
                        >
                            {workoutOptions.map(workoutType => (
                                <Menu.Item key={workoutType.toString()}>
                                    {({ active }) => (
                                        <MenuLink
                                            to={`/dashboard/${workoutType}`}
                                            active={active}
                                        >
                                            {capitalize(workoutType)}
                                        </MenuLink>
                                    )}
                                </Menu.Item>
                            ))}
                        </Menu.Items>
                    </MenuTransition>
                </>
            )}
        </Menu>
    );
};
