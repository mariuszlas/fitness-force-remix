module.exports = [
    {
        id: 'base',
        routes: [
            'register:success',
            'login:success',
            'users:current',
            'health:ok',
            'workouts-dashboard:success',
            'workouts-records:success',
            'workouts-list:success',
            'workout:success',
            'labels:success',
        ],
    },
];
