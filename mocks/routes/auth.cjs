module.exports = [
    {
        id: 'register',
        url: '/v1/auth/register',
        method: 'POST',
        variants: [
            {
                id: 'success',
                type: 'json',
                options: {
                    status: 201,
                    body: {
                        createdAt: '2023-03-10T16:27:58.848+00:00',
                        email: 'test@email.com',
                        lastLogin: null,
                        loginCount: 0,
                        name: 'Test Name',
                        updatedAt: '2023-03-10T16:27:58.848+00:00',
                        verified: false,
                    },
                },
            },
        ],
    },
    {
        id: 'login',
        url: '/v1/auth/login',
        method: 'POST',
        variants: [
            {
                id: 'success',
                type: 'json',
                options: {
                    status: 200,
                    body: {
                        accessToken: 'testToken',
                        refreshToken: 'testRefreshToken',
                        tokenType: 'Bearer',
                    },
                },
            },
        ],
    },
];
