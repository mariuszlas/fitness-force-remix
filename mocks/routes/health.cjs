module.exports = [
    {
        id: 'health',
        url: '/',
        method: 'GET',
        variants: [
            {
                id: 'ok',
                type: 'json',
                options: {
                    status: 200,
                    body: {
                        status: 'Mock server is up and running',
                    },
                },
            },
        ],
    },
];
