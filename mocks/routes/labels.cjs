module.exports = [
    {
        id: 'labels',
        url: '/v1/labels',
        method: 'GET',
        variants: [
            {
                id: 'success',
                type: 'json',
                options: {
                    status: 200,
                    body: [
                        {
                            value: 'test 1',
                            color: '#2579d8',
                        },
                        {
                            value: 'geo',
                            color: '#b40813',
                        },
                    ],
                },
            },
        ],
    },
];
