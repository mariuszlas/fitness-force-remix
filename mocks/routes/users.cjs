module.exports = [
    {
        id: 'users',
        url: '/v1/users/current',
        method: 'GET',
        variants: [
            {
                id: 'current',
                type: 'json',
                options: {
                    status: 200,
                    body: {
                        email: 'test@email.com',
                        name: 'Test Name',
                        verified: false,
                        createdAt: '2023-08-16T19:22:57.000+00:00',
                        updatedAt: '2023-12-25T19:57:02.377+00:00',
                        lastLogin: '2023-12-25T19:57:02.348+00:00',
                        loginCount: 0,
                    },
                },
            },
        ],
    },
];
