module.exports = [
    {
        id: 'workout',
        url: '/v1/workouts/11',
        method: 'GET',
        variants: [
            {
                id: 'success',
                type: 'json',
                options: {
                    status: 200,
                    body: {
                        distance: 10.0,
                        duration: 3382.0,
                        timestamp: '2023-12-31T06:09:00.000+00:00',
                        utcOffset: 0,
                        notes: 'test notes',
                        type: 'running',
                        id: 11,
                        pace: 338.2,
                        speed: 10.644589000591367,
                        label: {
                            value: 'test 1',
                            color: '#2579d8',
                        },
                        trajectory: null,
                        hasTrajectory: false,
                    },
                },
            },
        ],
    },
];
