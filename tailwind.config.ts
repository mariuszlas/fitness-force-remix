import type { Config } from 'tailwindcss';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const themes = require('daisyui/src/theming/themes');

export default {
    content: ['./app/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            maxWidth: {
                '8xl': '90rem',
            },
        },
    },
    daisyui: {
        themes: [
            {
                light: {
                    ...themes['[data-theme=light]'],
                    primary: '#14b8a6', // teal-500
                    // primary: '#2dd4bf', // teal-400

                    '--rounded-btn': '0.75rem',
                    '--btn-text-case': 'capitalize',
                    'primary-content': '#1f2937',
                },
            },
            {
                dark: {
                    ...themes['[data-theme=dark]'],
                    primary: '#14b8a6', // teal-500

                    '--rounded-btn': '0.75rem',
                    '--btn-text-case': 'capitalize',
                },
            },
        ],
    },
    plugins: [require('daisyui')],
} satisfies Config;
