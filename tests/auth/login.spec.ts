import { test, expect } from '@playwright/test';
import { email, password } from '../testData/user';

test.describe('login page', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
    });

    test.describe('navbar', () => {
        test('has a create account link and navigates to signup page on click @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Create Account' })
                .click();
            await expect(page).toHaveURL('/signup');
        });

        test('has a logo link and navigates to the landing page on click @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Workout Odyssey' })
                .click();
            await expect(page).toHaveURL('/');
        });
    });

    test.describe('form', () => {
        test('has a sign up link and navigates to signup page on click @ci', async ({
            page,
        }) => {
            await page
                .getByTestId('login-form')
                .getByRole('link', { name: 'Sign up' })
                .click();
            await expect(page).toHaveURL('/signup');
        });

        test('has a forgot password link and navigates to password recovery page on click @ci', async ({
            page,
        }) => {
            await page
                .getByTestId('login-form')
                .getByRole('link', { name: 'Forgot Password?' })
                .click();
            await expect(page).toHaveURL('/password-reset');
        });

        test('password input toggles password visibility @ci', async ({
            page,
        }) => {
            const passwordInput = page.getByLabel('Password', { exact: true });
            const togglePasswordBtn = page.getByLabel(
                'toggle password visibility'
            );

            await passwordInput.fill(password);
            await expect(passwordInput).toHaveAttribute('type', 'password');

            await togglePasswordBtn.click();
            await expect(passwordInput).toHaveAttribute('type', 'text');

            await togglePasswordBtn.click();
            await expect(passwordInput).toHaveAttribute('type', 'password');
        });

        test.skip('logs in the user and redirects to dashboard page', async ({
            page,
        }) => {
            await page.getByLabel('Email').fill(email);
            await page.getByLabel('Password', { exact: true }).fill(password);

            await page.getByRole('button', { name: 'Sign In' }).click();

            await expect(page).toHaveURL('/dashboard/running');
        });
    });
});
