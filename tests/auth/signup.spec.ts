import { test, expect } from '@playwright/test';
import { email, name, password } from '../testData/user';

test.describe('signup page', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/signup');
    });

    test.describe('navbar', () => {
        test('has a sign in link and navigates to login page on click @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Sign in' })
                .click();
            await expect(page).toHaveURL('/login');
        });

        test('has a logo link and navigates to the landing page on click @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Workout Odyssey' })
                .click();
            await expect(page).toHaveURL('/');
        });
    });

    test.describe('form', () => {
        test('has a login link and navigates to login page on click @ci', async ({
            page,
        }) => {
            await page
                .getByTestId('signup-form')
                .getByRole('link', { name: 'Sign in' })
                .click();
            await expect(page).toHaveURL('/login');
        });

        test('password input toggles password and reapeat password visibility independently @ci', async ({
            page,
        }) => {
            const passwordInput = page.getByLabel('Password', { exact: true });
            const togglePassword = page.getByLabel(
                'toggle password visibility'
            );
            const passwordRepeat = page.getByLabel('Repeat Password', {
                exact: true,
            });

            await passwordInput.fill(password);
            await passwordRepeat.fill(password);

            await expect(passwordInput).toHaveAttribute('type', 'password');
            await expect(passwordRepeat).toHaveAttribute('type', 'password');

            await togglePassword.click();
            await expect(passwordInput).toHaveAttribute('type', 'text');
            await expect(passwordRepeat).toHaveAttribute('type', 'password');

            await togglePassword.click();
            await expect(passwordInput).toHaveAttribute('type', 'password');
            await expect(passwordRepeat).toHaveAttribute('type', 'password');
        });

        test.skip('signs up the user and redirects to login page', async ({
            page,
        }) => {
            await page.getByLabel('Email').fill(email);
            await page.getByLabel('Name').fill(name);
            await page.getByLabel('Password', { exact: true }).fill(password);
            await page
                .getByLabel('Repeat Password', {
                    exact: true,
                })
                .fill(password);

            await page.getByRole('button', { name: 'Create Account' }).click();

            await expect(page).toHaveURL('/login');
        });
    });
});
