import { test, expect } from '@playwright/test';
import { email, password } from '../testData/user';

test.describe.skip('account menu', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
        await page.getByRole('button', { name: /account menu/i }).click();
    });

    test('has the user name, email and 3 other menu items', async ({
        page,
    }) => {
        await expect(page.getByRole('menu')).toBeVisible();
        await expect(page.getByTitle('user name')).toHaveText('Test Name');
        await expect(page.getByTitle('user email')).toHaveText(
            'test@email.com'
        );
        await expect(page.getByRole('menuitem')).toHaveCount(3);
    });

    test('has the best results button that opens up the best results modal', async ({
        page,
    }) => {
        const btn = page.getByRole('menuitem', {
            name: /best results/i,
        });
        await expect(btn).toBeVisible();
        await btn.click();

        await expect(page).toHaveURL('/dashboard/running/best-results');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).toBeVisible();
    });

    test('has the account settings button that opens up the account settings modal', async ({
        page,
    }) => {
        const btn = page.getByRole('menuitem', {
            name: /account settings/i,
        });
        await expect(btn).toBeVisible();
        await btn.click();

        await expect(page).toHaveURL('/dashboard/running/user');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).toBeVisible();
    });

    test('has the sign out button that logs the user out and navigates to the login page', async ({
        page,
    }) => {
        const btn = page.getByRole('menuitem', {
            name: /sign out/i,
        });
        await expect(btn).toBeVisible();
        await btn.click();

        await expect(page).toHaveURL('/login');
    });

    test('is closed when clicked on the account menu button again', async ({
        page,
    }) => {
        await expect(page.getByRole('menu')).toBeVisible();

        await page.getByRole('button', { name: /account menu/i }).click();

        await expect(page.getByRole('menu')).not.toBeVisible();
    });

    test('is closed when clicked outside of the menu', async ({ page }) => {
        await expect(page.getByRole('menu')).toBeVisible();

        await page.getByTestId('logo').first().click();

        await expect(page.getByRole('menu')).not.toBeVisible();
    });
});
