import { test, expect } from '@playwright/test';
import { email, password } from '../testData/user';

test.describe.skip('account settings modal', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
        await page.getByRole('button', { name: /account menu/i }).click();
        await page
            .getByRole('menuitem', {
                name: /best results/i,
            })
            .click();

        await expect(page).toHaveURL('/dashboard/running/best-results');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).toBeVisible();
    });

    test('has a correct heading', async ({ page }) => {
        await expect(page.getByRole('heading')).toHaveText(
            'Best Results for running'
        );
    });

    test('is closed by the close icon button in the header', async ({
        page,
    }) => {
        await page.getByRole('button', { name: 'Close' }).first().click();

        await expect(page).toHaveURL('/dashboard/running');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).not.toBeVisible();
    });

    test('is closed by the close button in the footer', async ({ page }) => {
        await page.getByText('Close').click();

        await expect(page).toHaveURL('/dashboard/running');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).not.toBeVisible();
    });

    test('has 5 workout records', async ({ page }) => {
        await expect(page.getByRole('listitem')).toHaveCount(5);
    });
});
