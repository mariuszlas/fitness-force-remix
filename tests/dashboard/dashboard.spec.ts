import { test, expect, Locator } from '@playwright/test';
import { email, password } from 'tests/testData/user';

test.describe.skip('dashboard', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
    });

    test.describe('workout menu', () => {
        let workoutMenuBtn: Locator;

        test.beforeEach(async ({ page }) => {
            workoutMenuBtn = page.getByRole('button', { name: 'Running' });
        });

        test('opens up the workout menu with 2 other workout type options', async ({
            page,
        }) => {
            await workoutMenuBtn.click();
            await expect(workoutMenuBtn).toHaveText('Running');
            await expect(page.getByRole('menu')).toBeVisible();
            await expect(page.getByRole('menuitem')).toHaveCount(2);
        });

        test('is closed by clicking workout menu button again', async ({
            page,
        }) => {
            await workoutMenuBtn.click();
            await expect(page.getByRole('menu')).toBeVisible();
            await workoutMenuBtn.click();
            await expect(page.getByRole('menu')).not.toBeVisible();
        });

        test('is closed when clicked outside of the menu', async ({ page }) => {
            await workoutMenuBtn.click();
            await expect(page.getByRole('menu')).toBeVisible();
            await page.getByTestId('logo').first().click();
            await expect(page.getByRole('menu')).not.toBeVisible();
        });
    });

    test('has the chart section with correct header', async ({ page }) => {
        const chartSection = page.getByTestId('chart-section');
        const chart = page.getByLabel('chart');
        const chartHeader = page.getByTitle('chart-section-title');
        const chartYearSelector = page.getByTestId('data-year-selector');

        await expect(chartSection).toBeVisible();
        await expect(chart).toBeVisible();
        await expect(chartHeader).toHaveText('Overview');
        await expect(chartYearSelector).toBeVisible();
    });

    test("year selector 'next' and 'previous' buttons switch between different years and the total view", async ({
        page,
    }) => {
        const primaryStatsTitle = page.getByTitle(
            'primary-stats-section-title'
        );
        const secondaryStatsTitle = page.getByTitle(
            'secondary-stats-section-title'
        );
        const workoutListTitle = page.getByTitle('workout-list-section-title');
        const previousYearBtn = page.getByRole('button', {
            name: /select previous year/i,
        });
        const nextYearBtn = page.getByRole('button', {
            name: /select next year/i,
        });

        const date = new Date();
        const year = date.getFullYear();
        const month = date.toLocaleDateString('default', { month: 'long' });

        // check the default dashboard view
        await expect(primaryStatsTitle).toHaveText('Total');
        await expect(secondaryStatsTitle).toHaveText(`Year ${year}`);
        await expect(workoutListTitle).toHaveText('Workouts this month');

        // go to the previous year
        await previousYearBtn.click();

        await expect(primaryStatsTitle).toHaveText('Year 2023');
        await expect(secondaryStatsTitle).toHaveText('December 2023');
        await expect(workoutListTitle).toHaveText('Workouts in December 2023');

        // go to the last year
        await previousYearBtn.click({ clickCount: 4 });

        await expect(primaryStatsTitle).toHaveText('Year 2020');
        await expect(secondaryStatsTitle).toHaveText('June 2020');
        await expect(workoutListTitle).toHaveText('Workouts in June 2020');

        // try going to the previous year (no more previous years)
        await previousYearBtn.click();

        await expect(primaryStatsTitle).toHaveText('Year 2020');
        await expect(secondaryStatsTitle).toHaveText('June 2020');
        await expect(workoutListTitle).toHaveText('Workouts in June 2020');

        // go to the total view
        await nextYearBtn.click({ clickCount: 4 });

        await expect(primaryStatsTitle).toHaveText('Total');
        await expect(secondaryStatsTitle).toHaveText(
            `Year ${date.getFullYear()}`
        );
        await expect(workoutListTitle).toHaveText('Workouts this month');

        // go to the next year (no more years beyond total view)
        await nextYearBtn.click();

        await expect(primaryStatsTitle).toHaveText('Total');
        await expect(secondaryStatsTitle).toHaveText(
            `Year ${date.getFullYear()}`
        );
        await expect(workoutListTitle).toHaveText('Workouts this month');
    });

    test('year selector dropdown selector switches between different years and the total view', async ({
        page,
    }) => {
        const primaryStatsTitle = page.getByTitle(
            'primary-stats-section-title'
        );
        const secondaryStatsTitle = page.getByTitle(
            'secondary-stats-section-title'
        );
        const workoutListTitle = page.getByTitle('workout-list-section-title');
        const yearSelectorDropdown = page.getByTestId(
            'data-year-selector-dropdown'
        );
        const date = new Date();

        // select one of the years
        await yearSelectorDropdown.selectOption({ label: '2023' });

        await expect(primaryStatsTitle).toHaveText('Year 2023');
        await expect(secondaryStatsTitle).toHaveText('December 2023');
        await expect(workoutListTitle).toHaveText('Workouts in December 2023');

        // select the last year
        await yearSelectorDropdown.selectOption({ label: '2020' });

        await expect(primaryStatsTitle).toHaveText('Year 2020');
        await expect(secondaryStatsTitle).toHaveText('June 2020');
        await expect(workoutListTitle).toHaveText('Workouts in June 2020');

        // select the toal view
        await yearSelectorDropdown.selectOption({ label: 'Total' });

        await expect(primaryStatsTitle).toHaveText('Total');
        await expect(secondaryStatsTitle).toHaveText(
            `Year ${date.getFullYear()}`
        );
        await expect(workoutListTitle).toHaveText('Workouts this month');
    });
});
