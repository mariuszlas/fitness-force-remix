import { test, expect } from '@playwright/test';
import { email, password } from 'tests/testData/user';

test.describe.skip('workout details', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
        await page
            .getByTestId('data-year-selector-dropdown')
            .selectOption({ label: '2023' });

        await page
            .getByTestId('workout-list-section')
            .getByRole('listitem')
            .first()
            .getByLabel('workout 11 properties menu')
            .click();

        await page.getByRole('menuitem', { name: 'Workout Details' }).click();
    });

    test('has a correct heading', async ({ page }) => {
        await expect(
            page
                .getByTestId('drawer-container')
                .getByRole('dialog')
                .getByRole('heading')
        ).toHaveText('Workout Details');
        await expect(page).toHaveURL('/dashboard/running/11');
    });

    test('is closed by the close icon button in the header', async ({
        page,
    }) => {
        await page.getByRole('button', { name: 'Close' }).click();

        await expect(page).toHaveURL('/dashboard/running');
        await expect(
            page.getByTestId('drawer-container').getByRole('dialog')
        ).not.toBeVisible();
    });

    test('has a list of items', async ({ page }) => {
        await expect(page.getByRole('listitem')).toHaveCount(6);
        await expect(page.getByTestId('workout-map')).not.toBeVisible();
    });
});
