import { test, expect, Locator } from '@playwright/test';
import { email, password } from 'tests/testData/user';

test.describe.skip('dashboard', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
    });

    test.describe('workout list', () => {
        let firstPageBtn: Locator;
        let secondPageBtn: Locator;
        let lastPageBtn: Locator;
        let workoutListSection: Locator;
        let allWorkoutsToggle: Locator;
        let workouts: Locator;

        test.beforeEach(async ({ page }) => {
            workoutListSection = page.getByTestId('workout-list-section');
            allWorkoutsToggle = workoutListSection.getByLabel('All Workouts');
            workouts = workoutListSection.getByRole('listitem');
            firstPageBtn = workoutListSection.getByRole('button', {
                name: '1',
                exact: true,
            });
            secondPageBtn = workoutListSection.getByRole('button', {
                name: '2',
                exact: true,
            });
            lastPageBtn = workoutListSection.getByRole('button', {
                name: '3',
                exact: true,
            });
        });

        test('switches between all workout and monthly workout list views', async ({
            page,
        }) => {
            const heading = workoutListSection.getByRole('heading');

            await expect(workouts).toHaveCount(0);
            await expect(heading).toHaveText('Workouts this month');
            await expect(page.getByTestId('no-workouts-message')).toBeVisible();

            // toggle all workouts view
            await allWorkoutsToggle.click();

            await expect(workouts).toHaveCount(10);
            await expect(heading).toHaveText('All training records');

            // toggle default monthly view again
            await allWorkoutsToggle.click();

            await expect(workouts).toHaveCount(0);
            await expect(heading).toHaveText('Workouts this month');
            await expect(page.getByTestId('no-workouts-message')).toBeVisible();
        });

        test('shows 10 workout records on page 1 and 2 records on page 2 and switches between monthly pages', async ({
            page,
        }) => {
            await page
                .getByTestId('data-year-selector-dropdown')
                .selectOption({ label: '2023' });
            await expect(workouts).toHaveCount(10);

            // switch to the second page of current month workouts
            await secondPageBtn.click();
            await expect(workouts).toHaveCount(2);

            // show the first page of current month workouts again
            await firstPageBtn.click();
            await expect(workouts).toHaveCount(10);
        });

        test('shows 10 workout records on page 1 and 9 records on the last page', async ({
            page,
        }) => {
            // toggle all workouts view
            await allWorkoutsToggle.click();
            await expect(workouts).toHaveCount(10);

            // go the page no 3 (last one)
            await lastPageBtn.click();
            await expect(workouts).toHaveCount(9);

            // go back to the first page
            await firstPageBtn.click();
            await expect(workouts).toHaveCount(10);
        });

        test.describe('workout properties menu', () => {
            let propertiesMenuBtn: Locator;

            test.beforeEach(async ({ page }) => {
                await page
                    .getByTestId('data-year-selector-dropdown')
                    .selectOption({ label: '2023' });

                const workout = workoutListSection
                    .getByRole('listitem')
                    .first();
                propertiesMenuBtn = workout.getByLabel(
                    'workout 11 properties menu'
                );

                await propertiesMenuBtn.click();
            });

            test('has 3 menu items', async ({ page }) => {
                await expect(page.getByRole('menu')).toBeVisible();
                await expect(page.getByRole('menuitem')).toHaveCount(3);
            });

            test('is closed when properties menu button is clicked again', async ({
                page,
            }) => {
                await expect(page.getByRole('menu')).toBeVisible();
                await propertiesMenuBtn.click();
                await expect(page.getByRole('menu')).not.toBeVisible();
            });

            test('is closed when clicked outside of the menu', async ({
                page,
            }) => {
                await expect(page.getByRole('menu')).toBeVisible();
                await page.getByTestId('logo').first().click();
                await expect(page.getByRole('menu')).not.toBeVisible();
            });
        });
    });
});
