import { test, expect } from '@playwright/test';
import { email, password } from '../testData/user';

test.describe.skip('workout upload modal', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/login');
        await page.getByLabel('Email').fill(email);
        await page.getByLabel('Password', { exact: true }).fill(password);
        await page.getByRole('button', { name: 'Sign In' }).click();
        await page.getByRole('link', { name: 'add workout' }).click();
    });

    test('has a correct heading', async ({ page }) => {
        await expect(
            page
                .getByTestId('modal-container')
                .getByRole('dialog')
                .getByRole('heading')
        ).toHaveText('Data Upload');
        await expect(page).toHaveURL('/dashboard/running/new');
    });

    test('is closed by the close icon button in the header', async ({
        page,
    }) => {
        await page.getByRole('button', { name: 'Close' }).first().click();

        await expect(page).toHaveURL('/dashboard/running');
        await expect(
            page.getByTestId('modal-container').getByRole('dialog')
        ).not.toBeVisible();
    });
});
