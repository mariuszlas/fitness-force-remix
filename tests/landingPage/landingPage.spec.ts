import { test, expect } from '@playwright/test';

test.describe('landing page', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/');
    });

    test('has hero title @ci', async ({ page }) => {
        await expect(page).toHaveTitle(/Workout Odyssey/);
    });

    test('has a get started CTA that navigates to login page @ci', async ({
        page,
    }) => {
        await page.getByRole('button', { name: 'Get Started' }).click();
        await expect(page).toHaveURL('/login');
    });

    test('has 4 main images @ci', async ({ page }) => {
        await expect(page.locator('main').getByRole('img')).toHaveCount(4);
    });

    test.describe('navbar', () => {
        test('has a sign in link that navigates to login page @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Sign in' })
                .click();
            await expect(page).toHaveURL('/login');
        });

        test('has a create account link that navigates to sign up page @ci', async ({
            page,
        }) => {
            await page
                .getByRole('navigation')
                .getByRole('link', { name: 'Create Account' })
                .click();
            await expect(page).toHaveURL('/signup');
        });

        test('has a theme toggle that switches between light and dark themes @ci', async ({
            page,
        }) => {
            const html = page.locator('html');
            const themeSwith = page
                .getByRole('navigation')
                .getByLabel('theme-switch');

            await expect(themeSwith).toBeVisible();
            await expect(html).toHaveAttribute('data-theme', 'light');

            await themeSwith.click();
            await expect(html).toHaveAttribute('data-theme', 'dark');

            await themeSwith.click();
            await expect(html).toHaveAttribute('data-theme', 'light');
        });
    });

    test.describe('footer', () => {
        test('has a copyright note and a link to GitLab @ci', async ({
            page,
        }) => {
            await expect(page.getByLabel('copyright note')).toBeVisible();
            await expect(
                page.getByLabel("Visit the project's GitLab page")
            ).toBeVisible();
        });
    });
});
