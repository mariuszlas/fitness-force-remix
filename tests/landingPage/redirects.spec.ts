import { test, expect } from '@playwright/test';

test.describe('redirects', () => {
    test('should redirect to landing page from /set-theme @ci', async ({
        page,
    }) => {
        await page.goto('/set-theme');
        await expect(page).toHaveTitle(/Workout Odyssey/);
        await expect(page).toHaveURL('/');
    });

    test('should redirect to landing page from /request-password-reset @ci', async ({
        page,
    }) => {
        await page.goto('/request-password-reset');
        await expect(page).toHaveTitle(/Workout Odyssey/);
        await expect(page).toHaveURL('/');
    });
});
