import { test, expect } from '@playwright/test';

test.describe('404 Not Found page', () => {
    test.beforeEach(async ({ page }) => {
        await page.goto('/non-existing-page-123456');
    });

    test('has the 404 Not found error message @ci', async ({ page }) => {
        await expect(page.getByRole('heading')).toHaveText('Page Not Found');
    });

    test('has a link that navigates to the landing page on click @ci', async ({
        page,
    }) => {
        await page.getByRole('link').click();
        await expect(page).toHaveURL('/');
    });
});
